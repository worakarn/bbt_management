﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BBT_Management
{
    public partial class main : Form
    {
        public static string conf_txt = "";
        private static int conf = 0;
        private string bc_type = "R";
        private int rssi_selt = (int)rssi_select.tag;
        private string rssi_selt_str = "";
        private int rssi_sort = (int)rssi_sort_by.rssi;
        private bool trigger = true;
        function bbt_fn = new function();
        ConfirmForm conf_form = new ConfirmForm();

        public main()
        {
            InitializeComponent();
        }

        private void main_Load(object sender, EventArgs e)
        {
            tb_hn.Text = "";
            cb_hos.Items.Add("พญาไท 2");
            cb_hos.Items.Add("พญาไท 3");
            cb_hos.Items.Add("พญาไท ศรีราชา");
            cb_hos.Items.Add("Demo 1");
            cb_hos.Items.Add("Demo 2");
        }

        private void cb_hos_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cb_hos.SelectedIndex == (int)hospital.py2)
            {
                cb_trans_from.Items.Clear();
                cb_trans_from.Items.Add("Nursery ชั้น 4");
                cb_trans_from.Items.Add("Nursery ชั้น 11");
                cb_trans_from.Items.Add("ห้องพักมารดา");
                cb_trans_from.Items.Add("อื่นๆ");

                cb_trans_to.Items.Clear();
                cb_trans_to.Items.Add("Nursery ชั้น 4");
                cb_trans_to.Items.Add("Nursery ชั้น 11");
                cb_trans_to.Items.Add("ห้องพักมารดา");
                cb_trans_to.Items.Add("อื่นๆ");

                cb_select_fl.Items.Clear();
                cb_select_fl.Items.Add("All");
                cb_select_fl.Items.Add("3");
                cb_select_fl.Items.Add("4");
                cb_select_fl.Items.Add("11");
                cb_select_fl.Items.Add("12");
                cb_select_fl.Items.Add("15");

            }
            else if (cb_hos.SelectedIndex == (int)hospital.py3)
            {
                cb_trans_from.Items.Clear();
                cb_trans_from.Items.Add("Nursery ชั้น 6");
                cb_trans_from.Items.Add("Nursery ชั้น 7");
                cb_trans_from.Items.Add("ห้องพักมารดา");
                cb_trans_from.Items.Add("อื่นๆ");

                cb_trans_to.Items.Clear();
                cb_trans_to.Items.Add("Nursery ชั้น 6");
                cb_trans_to.Items.Add("Nursery ชั้น 7");
                cb_trans_to.Items.Add("ห้องพักมารดา");
                cb_trans_to.Items.Add("อื่นๆ");

                cb_select_fl.Items.Clear();
                cb_select_fl.Items.Add("All");
                cb_select_fl.Items.Add("3");
                cb_select_fl.Items.Add("6");
                cb_select_fl.Items.Add("7");
            }
            else if (cb_hos.SelectedIndex == (int)hospital.pys)
            {
                cb_trans_from.Items.Clear();
                cb_trans_from.Items.Add("Nursery ชั้น 2");
                cb_trans_from.Items.Add("Nursery ชั้น 7");
                cb_trans_from.Items.Add("ห้องพักมารดา");
                cb_trans_from.Items.Add("อื่นๆ");

                cb_trans_to.Items.Clear();
                cb_trans_to.Items.Add("Nursery ชั้น 2");
                cb_trans_to.Items.Add("Nursery ชั้น 7");
                cb_trans_to.Items.Add("ห้องพักมารดา");
                cb_trans_to.Items.Add("อื่นๆ");

                cb_select_fl.Items.Clear();
                cb_select_fl.Items.Add("All");
                cb_select_fl.Items.Add("2");
                cb_select_fl.Items.Add("7");
            }
            else if (cb_hos.SelectedIndex == (int)hospital.demo1)
            {
                cb_trans_from.Items.Clear();
                cb_trans_from.Items.Add("Nursery ชั้น 12");
                cb_trans_from.Items.Add("ห้องพักมารดา");
                cb_trans_from.Items.Add("อื่นๆ");

                cb_trans_to.Items.Clear();
                cb_trans_to.Items.Add("Nursery ชั้น 12");
                cb_trans_to.Items.Add("ห้องพักมารดา");
                cb_trans_to.Items.Add("อื่นๆ");

                cb_select_fl.Items.Clear();
                cb_select_fl.Items.Add("All");
                cb_select_fl.Items.Add("12");
            }
        }

        private void bt_dis_Click(object sender, EventArgs e)
        {
            conf = Check_confirm(bt_dis.Text);
            if(conf == 1)
            {
                string host = cvrt_Host_Indx2str(cb_hos.SelectedIndex);
                string db = cvrt_DB_Indx2str(cb_hos.SelectedIndex);
                int res = bbt_fn.Discharge(host, Int32.Parse(tb_port.Text), db, tb_hn.Text, tb_ord.Text);
                gen_Message(res);
            }
        }

        private void bt_cancel_dis_Click(object sender, EventArgs e)
        {
            conf = Check_confirm(bt_cancel_dis.Text);
            if (conf == 1)
            {
                string host = cvrt_Host_Indx2str(cb_hos.SelectedIndex);
                string db = cvrt_DB_Indx2str(cb_hos.SelectedIndex);
                int res = bbt_fn.Discharge_cancel(host, Int32.Parse(tb_port.Text), db, tb_hn.Text, tb_ord.Text);
                gen_Message(res);
            }
        }

        private void bt_trans_Click(object sender, EventArgs e)
        {
            conf = Check_confirm(bt_trans.Text);
            if (conf == 1)
            {
                pn_bc_mana.Visible = true;
                pn_trans.Visible = true;
            }
        }

        private void bt_edit_trans_Click(object sender, EventArgs e)
        {
            conf = Check_confirm(bt_edit_trans.Text);
            if (conf == 1)
            {
                string host = cvrt_Host_Indx2str(cb_hos.SelectedIndex);
                string db = cvrt_DB_Indx2str(cb_hos.SelectedIndex);
                int res = bbt_fn.Transition_edit(host, Int32.Parse(tb_port.Text), db, tb_hn.Text, tb_ord.Text);
                gen_Message(res);
            }
        }

        private void bt_tm_trans_Click(object sender, EventArgs e)
        {
            conf = Check_confirm(bt_tm_trans.Text);
            if (conf == 1)
            {
                string host = cvrt_Host_Indx2str(cb_hos.SelectedIndex);
                string db = cvrt_DB_Indx2str(cb_hos.SelectedIndex);
                int res = bbt_fn.Transition_extra_time(host, Int32.Parse(tb_port.Text), db, tb_hn.Text, tb_ord.Text);
                gen_Message(res);
            }
        }

        private void bt_cancel_trans_Click(object sender, EventArgs e)
        {
            conf = Check_confirm(bt_cancel_trans.Text);
            if (conf == 1)
            {
                string host = cvrt_Host_Indx2str(cb_hos.SelectedIndex);
                string db = cvrt_DB_Indx2str(cb_hos.SelectedIndex);
                int res = bbt_fn.Transition_cancel(host, Int32.Parse(tb_port.Text), db, tb_hn.Text, tb_ord.Text);
                gen_Message(res);
            }
        }

        private void bt_mute_alert_Click(object sender, EventArgs e)
        {
            conf = Check_confirm(bt_mute_alert.Text);
            if (conf == 1)
            {
                string host = cvrt_Host_Indx2str(cb_hos.SelectedIndex);
                string db = cvrt_DB_Indx2str(cb_hos.SelectedIndex);
                int res = bbt_fn.UIAlert_mute(host, Int32.Parse(tb_port.Text), db, tb_hn.Text, tb_ord.Text);
                gen_Message(res);
            }
        }

        private void bt_resolve_alert_Click(object sender, EventArgs e)
        {
            conf = Check_confirm(bt_resolve_alert.Text);
            if (conf == 1)
            {
                string host = cvrt_Host_Indx2str(cb_hos.SelectedIndex);
                string db = cvrt_DB_Indx2str(cb_hos.SelectedIndex);
                int res = bbt_fn.UIAlert_confirmd(host, Int32.Parse(tb_port.Text), db, tb_hn.Text, tb_ord.Text);
                gen_Message(res);
            }
        }

        private void bt_edit_tag_lost_Click(object sender, EventArgs e)
        {
            conf = Check_confirm(bt_edit_tag_lost.Text);
            if (conf == 1)
            {
                string host = cvrt_Host_Indx2str(cb_hos.SelectedIndex);
                string db = cvrt_DB_Indx2str(cb_hos.SelectedIndex);
                int res = bbt_fn.Tag_lost(host, Int32.Parse(tb_port.Text), db, tb_hn.Text, tb_ord.Text);
                gen_Message(res);
            }
        }

        private void bt_edit_tag_login_Click(object sender, EventArgs e)
        {
            conf = Check_confirm(bt_edit_tag_login.Text);
            if (conf == 1)
            {
                string host = cvrt_Host_Indx2str(cb_hos.SelectedIndex);
                string db = cvrt_DB_Indx2str(cb_hos.SelectedIndex);
                int res = bbt_fn.Tag_login(host, Int32.Parse(tb_port.Text), db, tb_hn.Text, tb_ord.Text);
                gen_Message(res);
            }
        }

        private void bt_edit_tag_sn_Click(object sender, EventArgs e)
        {
            int cnf_hos = Check_hos_confirm();
            if (cnf_hos == 1)
            {
                pn_bc_mana.Visible = true;
                pn_trans.Visible = true;
                pn_check_R_G.Visible = true;
                pn_check_rssi.Visible = true;
                pn_edit.Visible = true;

                rb_edit_sn_tag.Checked = true;
                tb_ord_edit_hn_from.Enabled = false;
                tb_ord_edit_hn_change.Enabled = false;
            }
        }

        private void bt_edit_hn_child_Click(object sender, EventArgs e)
        {
            int cnf_hos = Check_hos_confirm();
            if (cnf_hos == 1)
            {
                pn_bc_mana.Visible = true;
                pn_trans.Visible = true;
                pn_check_R_G.Visible = true;
                pn_check_rssi.Visible = true;
                pn_edit.Visible = true;

                rb_edit_hn_child.Checked = true;
                tb_ord_edit_hn_from.Enabled = true;
                tb_ord_edit_hn_change.Enabled = true;
            }
        }

        private void rb_edit_hn_child_Click(object sender, EventArgs e)
        {
            tb_ord_edit_hn_from.Enabled = true;
            tb_ord_edit_hn_change.Enabled = true;
        }

        private void rb_edit_sn_tag_Click(object sender, EventArgs e)
        {
            tb_ord_edit_hn_from.Enabled = false;
            tb_ord_edit_hn_change.Enabled = false;
        }

        private void bt_edit_ok_Click(object sender, EventArgs e)
        {
            int cfm_edit = Check_edit_confirm();
            string host = cvrt_Host_Indx2str(cb_hos.SelectedIndex);
            string db = cvrt_DB_Indx2str(cb_hos.SelectedIndex);
            if (rb_edit_sn_tag.Checked == true && cfm_edit == 2)
            {
                int res = bbt_fn.Edit_serial_tag(host, Int32.Parse(tb_port.Text), db, tb_edit_from.Text, tb_edit_change.Text);
                gen_Message(res);
            }
            else if(rb_edit_hn_child.Checked == true && cfm_edit == 4)
            {
                int res = bbt_fn.Edit_HN_child(host, Int32.Parse(tb_port.Text), db, tb_edit_from.Text, tb_ord_edit_hn_from.Text, tb_edit_change.Text, tb_ord_edit_hn_change.Text);
                gen_Message(res);
            }
        }

        private void bt_edit_back_Click(object sender, EventArgs e)
        {
            pn_bc_mana.Visible = false;
            pn_trans.Visible = false;
            pn_check_R_G.Visible = false;
            pn_check_rssi.Visible = false;
            pn_edit.Visible = false;
        }

        private void bt_edit_room_Click(object sender, EventArgs e)
        {
            conf = Check_confirm(bt_edit_room.Text);
            if (conf == 1)
            {
                pn_bc_mana.Visible = true;
                pn_trans.Visible = true;
                pn_check_R_G.Visible = true;
                pn_check_rssi.Visible = true;
                pn_edit.Visible = true;
                pn_edit_room.Visible = true;
            }
        }

        private void bt_edit_room_back_Click(object sender, EventArgs e)
        {
            pn_bc_mana.Visible = false;
            pn_trans.Visible = false;
            pn_check_R_G.Visible = false;
            pn_check_rssi.Visible = false;
            pn_edit.Visible = false;
            pn_edit_room.Visible = false;
        }

        private void bt_edit_room_ok_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(tb_edit_room.Text) && !String.IsNullOrEmpty(tb_edit_room_fl.Text))
            {
                string host = cvrt_Host_Indx2str(cb_hos.SelectedIndex);
                string db = cvrt_DB_Indx2str(cb_hos.SelectedIndex);
                int res = bbt_fn.Room_edit(host, Int32.Parse(tb_port.Text), db, tb_hn.Text, tb_ord.Text, tb_edit_room.Text, tb_edit_room_fl.Text);
                gen_Message(res);
            }
            else
            {
                MessageBox.Show("กรุณาระบุข้อมูลให้ครบถ้วน");
            }
        }

        private void bt_edit_room_out_Click(object sender, EventArgs e)
        {
            conf = Check_confirm(bt_edit_room_out.Text);
            if (conf == 1)
            {
                string host = cvrt_Host_Indx2str(cb_hos.SelectedIndex);
                string db = cvrt_DB_Indx2str(cb_hos.SelectedIndex);
                int res = bbt_fn.Room_out_edit(host, Int32.Parse(tb_port.Text), db, tb_hn.Text, tb_ord.Text);
                gen_Message(res);
            }
        }
        
        private void bt_clear_col_db_Click(object sender, EventArgs e)
        {
            int cnf_hos = Check_hos_confirm();
            if (cnf_hos == 1)
            {
                string host = cvrt_Host_Indx2str(cb_hos.SelectedIndex);
                string db = cvrt_DB_Indx2str(cb_hos.SelectedIndex);
                int res = bbt_fn.Clear_DB(host, Int32.Parse(tb_port.Text), db);
                gen_Message(res);
            }
        }

        private void bt_bc_mana_Click(object sender, EventArgs e)
        {
            int cnf_hos = Check_hos_confirm();
            if(cnf_hos == 1)
            {
                pn_bc_mana.Visible = true;
                pn_trans.Visible = false;
            }
        }

        private void bt_back_home_Click(object sender, EventArgs e)
        {
            pn_bc_mana.Visible = false;
            pn_trans.Visible = false;
        }

        private void bt_OK_trans_Click(object sender, EventArgs e)
        {
            if (cb_trans_from.SelectedIndex != -1 && cb_trans_to.SelectedIndex != -1)
            {
                string host = cvrt_Host_Indx2str(cb_hos.SelectedIndex);
                string db = cvrt_DB_Indx2str(cb_hos.SelectedIndex);
                int res = bbt_fn.Transition(host, Int32.Parse(tb_port.Text), db, tb_hn.Text, tb_ord.Text, cb_hos.SelectedIndex, cb_trans_from.SelectedIndex, cb_trans_to.SelectedIndex);
                gen_Message(res);
            }
            else if(cb_trans_from.SelectedIndex == -1)
            {
                MessageBox.Show("กรุณาเลือกสถานที่เริ่มต้น");
            }
            else if (cb_trans_to.SelectedIndex == -1)
            {
                MessageBox.Show("กรุณาเลือกสถานที่ปลายทาง");
            }
        }

        private void bt_back_home_trans_Click(object sender, EventArgs e)
        {
            pn_bc_mana.Visible = false;
            pn_trans.Visible = false;
        }

        private void bt_check_led_and_sound_Click(object sender, EventArgs e)
        {
            pn_trans.Visible = true;
            pn_check_R_G.Visible = true;
        }

        private void rb_b_CheckedChanged(object sender, EventArgs e)
        {
            bc_type = rb_b.Text;
            //cb_select_fl.Items.Clear();
            cb_select_fl.Text = "-- Selected --";
            cb_select_fl.SelectedIndex = -1;
            cb_list_bc.Items.Clear();
            cb_list_bc.Text = "-- Selected --";
            cb_list_bc.SelectedIndex = -1;
        }

        private void rb_g_CheckedChanged(object sender, EventArgs e)
        {
            bc_type = rb_g.Text;
            //cb_select_fl.Items.Clear();
            cb_select_fl.Text = "-- Selected --";
            cb_select_fl.SelectedIndex = -1;
            cb_list_bc.Items.Clear();
            cb_list_bc.Text = "-- Selected --";
            cb_list_bc.SelectedIndex = -1;
        }

        private void rb_h_CheckedChanged(object sender, EventArgs e)
        {
            bc_type = rb_h.Text;
            //cb_select_fl.Items.Clear();
            cb_select_fl.Text = "-- Selected --";
            cb_select_fl.SelectedIndex = -1;
            cb_list_bc.Items.Clear();
            cb_list_bc.Text = "-- Selected --";
            cb_list_bc.SelectedIndex = -1;
        }

        private void rb_l_CheckedChanged(object sender, EventArgs e)
        {
            bc_type = rb_l.Text;
            //cb_select_fl.Items.Clear();
            cb_select_fl.Text = "-- Selected --";
            cb_select_fl.SelectedIndex = -1;
            cb_list_bc.Items.Clear();
            cb_list_bc.Text = "-- Selected --";
            cb_list_bc.SelectedIndex = -1;
        }

        private void rb_n_CheckedChanged(object sender, EventArgs e)
        {
            bc_type = rb_n.Text;
            //cb_select_fl.Items.Clear();
            cb_select_fl.Text = "-- Selected --";
            cb_select_fl.SelectedIndex = -1;
            cb_list_bc.Items.Clear();
            cb_list_bc.Text = "-- Selected --";
            cb_list_bc.SelectedIndex = -1;
        }

        private void rb_p_CheckedChanged(object sender, EventArgs e)
        {
            bc_type = rb_p.Text;
            //cb_select_fl.Items.Clear();
            cb_select_fl.Text = "-- Selected --";
            cb_select_fl.SelectedIndex = -1;
            cb_list_bc.Items.Clear();
            cb_list_bc.Text = "-- Selected --";
            cb_list_bc.SelectedIndex = -1;
        }

        private void rb_r_CheckedChanged(object sender, EventArgs e)
        {
            bc_type = rb_r.Text;
            //cb_select_fl.Items.Clear();
            cb_select_fl.Text = "-- Selected --";
            cb_select_fl.SelectedIndex = -1;
            cb_list_bc.Items.Clear();
            cb_list_bc.Text = "-- Selected --";
            cb_list_bc.SelectedIndex = -1;
        }

        private void rb_t_CheckedChanged(object sender, EventArgs e)
        {
            bc_type = rb_t.Text;
            //cb_select_fl.Items.Clear();
            cb_select_fl.Text = "-- Selected --";
            cb_select_fl.SelectedIndex = -1;
            cb_list_bc.Items.Clear();
            cb_list_bc.Text = "-- Selected --";
            cb_list_bc.SelectedIndex = -1;
        }

        private void rb_x_CheckedChanged(object sender, EventArgs e)
        {
            bc_type = rb_x.Text;
            //cb_select_fl.Items.Clear();
            cb_select_fl.Text = "-- Selected --";
            cb_select_fl.SelectedIndex = -1;
            cb_list_bc.Items.Clear();
            cb_list_bc.Text = "-- Selected --";
            cb_list_bc.SelectedIndex = -1;
        }

        private void cb_select_fl_SelectedIndexChanged(object sender, EventArgs e)
        {
            cb_list_bc.Items.Clear();
            cb_list_bc.Text = "-- Selected --";
            cb_list_bc.SelectedIndex = -1;
            
            string host = cvrt_Host_Indx2str(cb_hos.SelectedIndex);
            string db = cvrt_DB_Indx2str(cb_hos.SelectedIndex);
            List<beacon_location> list_bc = bbt_fn.Get_list_beacon(host, Int32.Parse(tb_port.Text), db, bc_type, cb_select_fl.Text);
            List<string> list_bc_sort = new List<string>();
            for(int i=0; i<list_bc.Count; i++)
            {
                list_bc_sort.Add(list_bc[i].beacon_serial);
            }
            list_bc_sort.Sort();
            for (int j = 0; j < list_bc_sort.Count; j++)
            {
                if( j == 0)
                {
                    cb_list_bc.Items.Add("All");
                }
                cb_list_bc.Items.Add(list_bc_sort[j]);
            }
        }

        private void bt_ok_or_stop_Click(object sender, EventArgs e)
        {
            conf = Check_confirm_SelectBC();
            if (conf == 1)
            {
                string host = cvrt_Host_Indx2str(cb_hos.SelectedIndex);
                string db = cvrt_DB_Indx2str(cb_hos.SelectedIndex);

                List<beacon_location> list_bc = new List<beacon_location>();
                List<string> bc = new List<string>();
                if (cb_list_bc.Text == "All")
                {
                    list_bc = bbt_fn.Get_list_beacon(host, Int32.Parse(tb_port.Text), db, bc_type, cb_select_fl.Text);
                    for (int i = 0; i < list_bc.Count; i++)
                    {
                        bc.Add(list_bc[i].beacon_serial);
                    }
                }
                else
                {
                    bc.Add(cb_list_bc.Text);
                }

                lb_show_led.Visible = true;
                lb_show_led.Text = "";

                if(rb_off_led.Checked == true || rb_off_sound.Checked == true || rb_reset.Checked == true)
                {
                    lb_show_led.Text = "";
                    for (int i=0; i<bc.Count; i++)
                    {
                        bbt_fn.Beacon_command_set(host, Int32.Parse(tb_port.Text), db, bc[i], bc_type, 1, 2, 1);
                    }
                }
                else if(rb_on_led.Checked == true && bt_ok_or_stop.DialogResult == DialogResult.OK)
                {
                    trigger = true;
                    loop_bc_work(host, db, bc, bc_type, 8, 9);
                    bt_ok_or_stop.Text = "STOP";
                    bt_ok_or_stop.DialogResult = DialogResult.Cancel;
                }
                else if(rb_on_sound.Checked == true && bt_ok_or_stop.DialogResult == DialogResult.OK)
                {
                    for (int i = 0; i < bc.Count; i++)
                    {
                        bbt_fn.Beacon_command_set(host, Int32.Parse(tb_port.Text), db, bc[i], bc_type, 1, 1, 1);

                    }
                    bt_ok_or_stop.Text = "STOP";
                    bt_ok_or_stop.DialogResult = DialogResult.Cancel;
                }
                else if(rb_on_work.Checked == true && bt_ok_or_stop.DialogResult == DialogResult.OK)
                {
                    if(rb_x.Checked == true)
                    {
                        for (int i = 0; i < bc.Count; i++)
                        {
                            bbt_fn.Beacon_command_set(host, Int32.Parse(tb_port.Text), db, bc[i], bc_type, 1, 1, 1);

                        }
                        bt_ok_or_stop.Text = "STOP";
                        bt_ok_or_stop.DialogResult = DialogResult.Cancel;
                    }
                    else if(rb_r.Checked == true)
                    {
                        trigger = true;
                        loop_bc_work(host, db, bc, bc_type, 8, 9);
                        bt_ok_or_stop.Text = "STOP";
                        bt_ok_or_stop.DialogResult = DialogResult.Cancel;
                    }
                    else if(rb_g.Checked == true)
                    {
                        trigger = true;
                        loop_bc_work(host, db, bc, bc_type, 5, 4);
                        bt_ok_or_stop.Text = "STOP";
                        bt_ok_or_stop.DialogResult = DialogResult.Cancel;

                    }
                    else if(rb_b.Checked == true || rb_h.Checked == true || rb_l.Checked == true || rb_n.Checked == true || rb_p.Checked == true || rb_t.Checked == true)
                    {
                        for (int i = 0; i < bc.Count; i++)
                        {
                            bbt_fn.Beacon_command_set(host, Int32.Parse(tb_port.Text), db, bc[i], bc_type, 1, 2, 1);
                        }
                    }
                }
                else if((rb_on_led.Checked == true || rb_on_sound.Checked == true || rb_on_work.Checked == true) && bt_ok_or_stop.DialogResult == DialogResult.Cancel)
                {
                    trigger = false;
                    for (int i = 0; i < bc.Count; i++)
                    {
                        bbt_fn.Beacon_command_set(host, Int32.Parse(tb_port.Text), db, bc[i], bc_type, 1, 2, 1);
                    }
                    bt_ok_or_stop.Text = "OK";
                    bt_ok_or_stop.DialogResult = DialogResult.OK;
                    lb_show_led.Text = "";
                }
            }
        }

        private void bt_back_select_fl_Click(object sender, EventArgs e)
        {
            pn_bc_mana.Visible = true;
            pn_trans.Visible = false;
            pn_check_R_G.Visible = false;
        }

        async void bt_check_rssi_Click(object sender, EventArgs e)
        {
            pn_trans.Visible = true;
            pn_check_R_G.Visible = true;
            pn_check_rssi.Visible = true;

            string host = cvrt_Host_Indx2str(cb_hos.SelectedIndex);
            string db = cvrt_DB_Indx2str(cb_hos.SelectedIndex);
            List<beacon_location> list_bc_loc = bbt_fn.Get_all_list_beacon_location(host, Int32.Parse(tb_port.Text), db);
            List<RSSI_checking> list_data_rssi = new List<RSSI_checking>();
            List<string> list_selt = new List<string>();
            List<Data> list_data = new List<Data>();
            int trg = 0;

            while (trigger == true)
            {
                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();

                list_data = bbt_fn.Get_Data(host, Int32.Parse(tb_port.Text), db);

                if (cb_rssi_select_bc.SelectedIndex == -1 && cb_rssi_select_tag.SelectedIndex == -1)
                {
                    cb_rssi_select_bc.Items.Clear();
                    cb_rssi_select_tag.Items.Clear();
                    list_selt = bbt_fn.Sort_select_bc_or_tag(list_data, rssi_selt);
                    if (rssi_selt == (int)rssi_select.beacon)
                    {
                        for (int i = 0; i < list_selt.Count; i++)
                        {
                            cb_rssi_select_bc.Items.Add(list_selt[i]);
                        }
                    }
                    else if (rssi_selt == (int)rssi_select.tag)
                    {
                        for (int i = 0; i < list_selt.Count; i++)
                        {
                            cb_rssi_select_tag.Items.Add(list_selt[i]);
                        }
                    }
                }
                else if (cb_rssi_select_bc.SelectedIndex != -1 || cb_rssi_select_tag.SelectedIndex != -1)
                {
                    if (rssi_selt == (int)rssi_select.beacon)
                    {
                        rssi_selt_str = cb_rssi_select_bc.Text;
                    }
                    else if (rssi_selt == (int)rssi_select.tag)
                    {
                        rssi_selt_str = cb_rssi_select_tag.Text;
                    }

                    list_data_rssi = bbt_fn.RSSI_sort_by(list_data, list_bc_loc, rssi_selt, rssi_selt_str, rssi_sort);

                    dg_rssi.Rows.Clear();
                    for (int i = 0; i < list_data_rssi.Count; i++)
                    {
                        dg_rssi.Rows.Add();
                        dg_rssi.Rows[i].Cells[0].Value = list_data_rssi[i].bc_serial;
                        dg_rssi.Rows[i].Cells[1].Value = list_data_rssi[i].tag_serial;
                        dg_rssi.Rows[i].Cells[2].Value = list_data_rssi[i].rssi;
                        dg_rssi.Rows[i].Cells[3].Value = list_data_rssi[i].evnt;
                        dg_rssi.Rows[i].Cells[4].Value = list_data_rssi[i].code;
                    }
                }
                
                if (trg == 0)
                {
                    lb_tic_toc.Text = "tic : " + list_selt.Count.ToString();
                    trg = 1;
                }
                else if (trg == 1)
                {
                    lb_tic_toc.Text = "toc : " + list_selt.Count.ToString();
                    trg = 0;
                }

                stopwatch.Stop();
                lb_timer.Text = stopwatch.ElapsedMilliseconds.ToString() + " ms";

                await System.Threading.Tasks.Task.Delay(TimeSpan.FromSeconds(2));
            }
        }

        private void rb_rssi_select_bc_CheckedChanged(object sender, EventArgs e)
        {
            rssi_selt = (int)rssi_select.beacon;
            cb_rssi_select_bc.Enabled = true;
            cb_rssi_select_bc.Items.Clear();
            cb_rssi_select_bc.Text = "-- Selected --";
            cb_rssi_select_bc.SelectedIndex = -1;
            cb_rssi_select_tag.Enabled = false;
            cb_rssi_select_tag.Items.Clear();
            cb_rssi_select_tag.Text = "-- Selected --";
            cb_rssi_select_tag.SelectedIndex = -1;
        }

        private void rb_rssi_select_tag_CheckedChanged(object sender, EventArgs e)
        {
            rssi_selt = (int)rssi_select.tag;
            cb_rssi_select_bc.Enabled = false;
            cb_rssi_select_bc.Items.Clear();
            cb_rssi_select_bc.Text = "-- Selected --";
            cb_rssi_select_bc.SelectedIndex = -1;
            cb_rssi_select_tag.Enabled = true;
            cb_rssi_select_tag.Items.Clear();
            cb_rssi_select_tag.Text = "-- Selected --";
            cb_rssi_select_tag.SelectedIndex = -1;
        }

        private void rb_sort_bc_CheckedChanged(object sender, EventArgs e)
        {
            rssi_sort = (int)rssi_sort_by.beacon;
        }

        private void rb_sort_tag_CheckedChanged(object sender, EventArgs e)
        {
            rssi_sort = (int)rssi_sort_by.tag;
        }

        private void rb_sort_rssi_CheckedChanged(object sender, EventArgs e)
        {
            rssi_sort = (int)rssi_sort_by.rssi;
        }

        private void rb_sort_code_CheckedChanged(object sender, EventArgs e)
        {
            rssi_sort = (int)rssi_sort_by.code;
        }

        private void bt_back_rssi_Click(object sender, EventArgs e)
        {
            pn_bc_mana.Visible = true;
            pn_trans.Visible = false;
            pn_check_R_G.Visible = false;
            pn_check_rssi.Visible = false;
        }

        private void bt_change_bc_Click(object sender, EventArgs e)
        {
        }

        private void bt_change_gate_Click(object sender, EventArgs e)
        {
        }

        private void bt_add_bc_Click(object sender, EventArgs e)
        {
        }

        private void bt_add_gate_Click(object sender, EventArgs e)
        {
        }

        private int Check_confirm(string want_str)
        {
            int resp = 0;
            if (cb_hos.SelectedIndex != -1 && tb_hn.Text != "")
            {
                conf_txt = want_str + "\n\n" + "HN : " + tb_hn.Text + "   Order : " + tb_ord.Text + "\n\n" + "ใช่หรือไม่?";

                DialogResult conf_res = conf_form.ShowDialog();
                if (conf_res == DialogResult.OK)
                {
                    resp = 1;
                }
            }
            else
            {
                if (cb_hos.SelectedIndex == -1 && tb_hn.Text == "")
                {
                    MessageBox.Show("กรุณาระบุโรงพยาบาล และ HN");
                }
                else if (cb_hos.SelectedIndex == -1)
                {
                    MessageBox.Show("กรุณาระบุโรงพยาบาล");
                }
                else if (tb_hn.Text == "")
                {
                    MessageBox.Show("กรุณาระบุ HN");
                }
            }
            return resp;
        }

        private int Check_hos_confirm()
        {
            int resp = 0;
            if (cb_hos.SelectedIndex != -1)
            {
                resp = 1;
            }
            else
            {
                MessageBox.Show("กรุณาระบุโรงพยาบาล");
            }
            return resp;
        }

        private int Check_confirm_SelectBC()
        {
            int resp = 0;
            if (cb_list_bc.SelectedIndex != -1)
            {
                if(rb_on_led.Checked == true || rb_off_led.Checked == true || rb_on_sound.Checked == true || rb_off_sound.Checked == true || rb_on_work.Checked == true || rb_reset.Checked == true)
                {
                    resp = 1;
                }
                else
                {
                    MessageBox.Show("กรุณาระบุคำสั่ง");
                }
                
            }
            else
            {
                MessageBox.Show("กรุณาระบุ beacon");
            }
            return resp;
        }

        private int Check_edit_confirm()
        {
            int resp = 0;
            if (tb_edit_from.Text != "")
            {
                resp += 1;
            }
            else
            {
                MessageBox.Show("กรุณาระบุข้อมูลให้ครบถ้วน");
            }

            if (tb_edit_change.Text != "")
            {
                resp += 1;
            }
            else
            {
                MessageBox.Show("กรุณาระบุข้อมูลให้ครบถ้วน");
            }

            if (rb_edit_hn_child.Checked == true)
            {
                if (tb_ord_edit_hn_from.Text != "")
                {
                    resp += 1;
                }
                else
                {
                    MessageBox.Show("กรุณาระบุข้อมูลให้ครบถ้วน");
                }
                if(tb_ord_edit_hn_change.Text != "")
                {
                    resp += 1;
                }
                else
                {
                    MessageBox.Show("กรุณาระบุข้อมูลให้ครบถ้วน");
                }
            }

            return resp;
        }

        private string cvrt_Host_Indx2str(int indx)
        {
            string host = "";
            switch(indx)
            {
                case (int)hospital.py2:
                    host = "localhost";
                    break;
                case (int)hospital.py3:
                    host = "localhost";
                    break;
                case (int)hospital.pys:
                    host = "localhost";
                    break;
                default:
                    host = "localhost";
                    break;
            }
            return host;
        }

        private string cvrt_DB_Indx2str(int indx)
        {
            string db = "";
            switch (indx)
            {
                case (int)hospital.py2:
                    db = "BBT_PY2";
                    break;
                case (int)hospital.py3:
                    db = "BBT_PY3";
                    break;
                case (int)hospital.pys:
                    db = "BBT_PYS";
                    break;
                case (int)hospital.demo1:
                    db = "BBT_SVK";
                    break;
            }
            return db;
        }

        private void gen_Message(int resp)
        {
            if (resp == (int)status.stt_success)
            {
                MessageBox.Show("Success");
            }
            else
            {
                MessageBox.Show("Error");
            }
        }
        
        async void loop_bc_work(string host, string db, List<string> list_bc, string bc_type, int work_case_1, int work_case_2)
        {
            lb_show_led.Visible = true;
            lb_show_led.Text = "";

            int led = 0;
            while (trigger == true)
            {
                await System.Threading.Tasks.Task.Delay(TimeSpan.FromSeconds(5));

                if (led == 0)
                {
                    for (int i = 0; i < list_bc.Count; i++)
                    {
                        bbt_fn.Beacon_command_set(host, Int32.Parse(tb_port.Text), db, list_bc[i], bc_type, 1, work_case_1, 1);

                    }
                    lb_show_led.Text = "ON : GREEN";
                    led = 1;
                }
                else if(led == 1)
                {
                    for (int i = 0; i < list_bc.Count; i++)
                    {
                        bbt_fn.Beacon_command_set(host, Int32.Parse(tb_port.Text), db, list_bc[i], bc_type, 1, work_case_2, 1);

                    }
                    lb_show_led.Text = "ON : RED";
                    led = 0;
                }
            }

        }
    }
}
