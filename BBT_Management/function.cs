﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBT_Management;
using System.Net.Sockets;
using MongoDB.Bson;
using System.IO;
using Newtonsoft.Json;
using MongoDB.Bson.Serialization;

namespace BBT_Management
{
    class function
    {
        bbt_DB bbt_db = new bbt_DB();
        public int Discharge(string host, int port, string database, string hn, string order)
        {
            int res = (int)status.stt_error;
            if (order == "-") { order = "1"; }
            int ord = Int32.Parse(order);
            List<PatientDevice_Group> lis_pdg = bbt_db.get_PatientDeviceGroup_hn(host, port, database, hn, ord);
            List<Baby> lis_baby = bbt_db.get_Baby(host, port, database, hn, ord);
            List<Patient> lis_patient = bbt_db.get_Patient_Child(host, port, database, hn, ord);
            if (lis_pdg.Count > 0 && lis_baby.Count > 0 && lis_patient.Count > 0)
            {
                List<Patient> lis_patient_mom = bbt_db.get_Patient_by_Mom(host, port, database, lis_baby[0].Mother_HN);
                List<alert_log> lis_alert_res = bbt_db.get_AlertLog(host, port, database, lis_pdg[0].TagSerial);
                List<alert_events> lis_alert_log = bbt_db.get_UIAlert(host, port, database, lis_pdg[0].TagSerial);
                List<lis_state> lis_baby_state = bbt_db.get_BabyStateLog(host, port, database, hn, ord);
                List<Bed> lis_bed_child = bbt_db.get_Bed(host, port, database, hn, ord);
                List<Bed> lis_bed_mom = bbt_db.get_Bed(host, port, database, hn, (int)tag_role.mother);
                List<transition> lis_tran = bbt_db.get_Transition(host, port, database, lis_pdg[0].TagSerial);
                List<tag_zone> lis_tag_zone_log = bbt_db.get_TagZoneLog(host, port, database, lis_pdg[0].TagSerial);
                List<zone_log> lis_log_patient = bbt_db.get_UILogPatient(host, port, database, hn, ord);
                List<lis_ticket> lis_ticket = bbt_db.get_Ticket(host, port, database, lis_baby[0].Mother_HN);
                List<lis_vertwo_log> lis_vertwo_logs = bbt_db.get_Ver2Logs(host, port, database, lis_pdg[0].TagSerial);
                Discharge_create_obj(host, port, database, lis_patient_mom, lis_bed_mom, hn, ord, lis_pdg[0].TagSerial, lis_patient, lis_baby_state, lis_bed_child, lis_tran, lis_log_patient, lis_tag_zone_log, lis_alert_log, lis_alert_res, lis_ticket, lis_vertwo_logs);
                Discharge_delete_data(host, port, database, hn, ord, lis_pdg[0].TagSerial, lis_baby[0].Mother_HN);
                res = (int)status.stt_success;
            }

            return res;
        }

        private void Discharge_create_obj(string host, int port, string database, List<Patient> lis_patient_mom, List<Bed> lis_bed_mom, string hn, int ord, string sn, List<Patient> lis_patient_child, List<lis_state> lis_baby_state, List<Bed> lis_bed_child, List<transition> lis_tran, List<zone_log> lis_log_patient, List<tag_zone> lis_tag_zone_log, List<alert_events> lis_alert_log, List<alert_log> lis_alert_res, List<lis_ticket> lis_ticket, List<lis_vertwo_log> lis_vertwo_logs)
        {
            var ts = DateTime.Now; ;

            lis_mom obj_mom = new lis_mom();
            obj_mom.patient = lis_patient_mom;
            obj_mom.bed = lis_bed_mom;

            discharge obj_dis = new discharge();
            obj_dis.patient_hn = hn;
            obj_dis.order = ord;
            obj_dis.tag_serial = sn;
            obj_dis.timestamp = ts;
            obj_dis.nurse_name = "ระบบ";
            obj_dis.dep = "Above";
            obj_dis.mother = obj_mom;
            obj_dis.patient = lis_patient_child;
            obj_dis.state = lis_baby_state;
            obj_dis.bed = lis_bed_child;
            obj_dis.transition = lis_tran;
            obj_dis.zone_log = lis_log_patient;
            obj_dis.tag_zone_log = lis_tag_zone_log;
            obj_dis.alert = lis_alert_log;
            obj_dis.alert_resolve = lis_alert_res;
            obj_dis.ticket = lis_ticket;
            obj_dis.vertwo_log = lis_vertwo_logs;

            bbt_db.insert_one(host, port, database, "dt_discharge", obj_dis.ToBsonDocument());
        }

        private void Discharge_delete_data(string host, int port, string database, string hn, int ord, string sn, string hn_mom)
        {
            bbt_db.delete_Tag(host, port, database, sn);
            bbt_db.delete_PDG(host, port, database, sn);
            bbt_db.delete_Baby(host, port, database, hn, ord);
            bbt_db.delete_Patient(host, port, database, hn, ord);
            bbt_db.delete_AlertLog(host, port, database, sn);
            bbt_db.delete_BabyStateLog(host, port, database, hn, ord);
            bbt_db.delete_Bed(host, port, database, hn, ord);
            bbt_db.delete_TagZone(host, port, database, sn);
            bbt_db.delete_TagZoneLog(host, port, database, sn);
            bbt_db.delete_TagZoneLogTemp(host, port, database, sn);
            bbt_db.delete_Transition(host, port, database, sn);
            bbt_db.delete_UIAlert(host, port, database, sn);
            bbt_db.delete_UILogPatient(host, port, database, hn, ord);
            bbt_db.delete_Ticket(host, port, database, hn_mom);
            bbt_db.delete_Vertwolog(host, port, database, sn);

            List<Baby> lis_baby = bbt_db.get_Baby_w_momHN(host, port, database, hn_mom);
            if (lis_baby.Count < 1)
            {
                bbt_db.delete_Patient(host, port, database, hn_mom, (int)tag_role.mother);
                bbt_db.delete_Bed(host, port, database, hn_mom, (int)tag_role.mother);
                List<PatientDevice_Group> lis_pdg = bbt_db.get_PatientDeviceGroup_hn(host, port, database, hn_mom, (int)tag_role.mother);
                if (lis_pdg.Count > 0)
                {
                    bbt_db.delete_Tag(host, port, database, lis_pdg[0].TagSerial);
                }
                bbt_db.delete_PDG(host, port, database, hn_mom, (int)tag_role.mother);
            }
        }
        ////////////////////////////////////////////////////////////

        public int Discharge_cancel(string host, int port, string database, string hn, string order)
        {
            int res = (int)status.stt_error;
            if (order == "-") { order = "1"; }
            int ord = Int32.Parse(order);
            List<discharge> lis_discharge = bbt_db.get_Discharge(host, port, database, hn, ord);
            for(int ind=0; ind<lis_discharge.Count; ind++)
            {
                string tag_child = lis_discharge[ind].tag_serial;
                if ((lis_discharge.Count == 1) || (ind == (lis_discharge.Count - 1)))
                {
                    string tag_mom = lis_discharge[ind].tag_serial.Replace("CH", "MO");
                    string hn_mom = lis_discharge[ind].mother.patient[0].hn;
                    string hn_child = lis_discharge[ind].patient_hn;

                    List<Tag> lis_tag = new List<Tag>();
                    Tag obj_tag = new Tag();
                    obj_tag._id = new ObjectId();
                    obj_tag.Serial = tag_mom;
                    lis_tag.Add(obj_tag);
                    obj_tag = new Tag();
                    obj_tag._id = new ObjectId();
                    obj_tag.Serial = tag_child;
                    lis_tag.Add(obj_tag);
                    BsonDocument[] arr_tag = new BsonDocument[lis_tag.Count];
                    for (int i = 0; i < lis_tag.Count; i++) { arr_tag[i] = lis_tag[i].ToBsonDocument(); }
                    bbt_db.insert_many(host, port, database, "dt_tags", arr_tag);

                    List<Patient> lis_patient = new List<Patient>();
                    lis_patient.Add(lis_discharge[ind].mother.patient[0]);
                    lis_patient.Add(lis_discharge[ind].patient[0]);
                    BsonDocument[] arr_patient = new BsonDocument[lis_patient.Count];
                    for (int i = 0; i < lis_patient.Count; i++) { arr_patient[i] = lis_patient[i].ToBsonDocument(); }
                    bbt_db.insert_many(host, port, database, "dt_patient", arr_patient);

                    List<PatientDevice_Group> lis_pdg = new List<PatientDevice_Group>();
                    PatientDevice_Group obj_pdg_mom = PDG_create_obj(tag_mom, hn_mom, tag_role.mother, 0, 0, 0, "", 0, 0, 0);
                    lis_pdg.Add(obj_pdg_mom);
                    PatientDevice_Group obj_pdg_child = PDG_create_obj(tag_child, hn_child, tag_role.child, 0, lis_discharge[ind].order, 0, "", 0, 0, 0);
                    lis_pdg.Add(obj_pdg_child);
                    BsonDocument[] arr_pdg = new BsonDocument[lis_pdg.Count];
                    for (int i = 0; i < lis_pdg.Count; i++) { arr_pdg[i] = lis_pdg[i].ToBsonDocument(); }
                    bbt_db.insert_many(host, port, database, "dt_patient_device_groups", arr_pdg);

                    Baby obj_baby = Baby_create_obj(hn_child, hn_mom, 0, baby_status.active, lis_discharge[ind].order, 0, lis_discharge[ind].patient[0].birth_dt, DateTime.Now, (int)baby_state.normal_no_name, 0);
                    bbt_db.insert_one(host, port, database, "dt_baby", obj_baby.ToBsonDocument());
                }

                List<tag_zone> lis_tag_zone_log = new List<tag_zone>();
                for (int i = 0; i < lis_discharge[ind].tag_zone_log.Count; i++)
                {
                    lis_tag_zone_log.Add(lis_discharge[ind].tag_zone_log[i]);
                    if (i == (lis_discharge[ind].tag_zone_log.Count - 1))
                    {
                        tag_zone obj_tagzone = lis_discharge[ind].tag_zone_log[i];
                        tag_zone_log obj_log_temp = TagZoneLog_create_obj(lis_discharge[ind].tag_zone_log[i].tag_serial, lis_discharge[ind].tag_zone_log[i].beacon_serial, lis_discharge[ind].tag_zone_log[i].zone_zone_code, "", 0, lis_discharge[ind].tag_zone_log[i].timestamp);
                        BsonDocument[] arr_log = new BsonDocument[lis_tag_zone_log.Count];
                        for (int j = 0; j < lis_tag_zone_log.Count; j++) { arr_log[j] = lis_tag_zone_log[j].ToBsonDocument(); }
                        bbt_db.insert_many(host, port, database, "dt_tag_zone_logs", arr_log);
                        bbt_db.insert_one(host, port, database, "dt_tag_zone", obj_tagzone.ToBsonDocument());
                        bbt_db.insert_one(host, port, database, "dt_tag_zone_logs_temp", obj_log_temp.ToBsonDocument());
                    }
                }

                List<alert_events> lis_alert = new List<alert_events>();
                for (int i = 0; i < lis_discharge[ind].alert.Count; i++) { lis_alert.Add(lis_discharge[ind].alert[i]); }
                BsonDocument[] arr_alt = new BsonDocument[lis_alert.Count];
                for (int j = 0; j < lis_alert.Count; j++) { arr_alt[j] = lis_alert[j].ToBsonDocument(); }
                bbt_db.insert_many(host, port, database, "ui_alerts", arr_alt);

                List<alert_log> lis_alert_res = new List<alert_log>();
                for (int i = 0; i < lis_discharge[ind].alert_resolve.Count; i++) { lis_alert_res.Add(lis_discharge[ind].alert_resolve[i]); }
                BsonDocument[] arr_alt_res = new BsonDocument[lis_alert_res.Count];
                for (int j = 0; j < lis_alert_res.Count; j++) { arr_alt_res[j] = lis_alert_res[j].ToBsonDocument(); }
                bbt_db.insert_many(host, port, database, "dt_alert_log", arr_alt_res);

                List<zone_log> lis_zone_log = new List<zone_log>();
                for (int i = 0; i < lis_discharge[ind].zone_log.Count; i++) { lis_zone_log.Add(lis_discharge[ind].zone_log[i]); }
                BsonDocument[] arr_zone_log = new BsonDocument[lis_zone_log.Count];
                for (int j = 0; j < lis_zone_log.Count; j++) { arr_zone_log[j] = lis_zone_log[j].ToBsonDocument(); }
                bbt_db.insert_many(host, port, database, "ui_log_patient", arr_zone_log);

                List<transition> lis_tran = new List<transition>();
                for (int i = 0; i < lis_discharge[ind].transition.Count; i++) { lis_tran.Add(lis_discharge[ind].transition[i]); }
                BsonDocument[] arr_tran = new BsonDocument[lis_tran.Count];
                for (int j = 0; j < lis_tran.Count; j++) { arr_tran[j] = lis_tran[j].ToBsonDocument(); }
                bbt_db.insert_many(host, port, database, "dt_transition", arr_tran);

                List<lis_state> lis_ste = new List<lis_state>();
                for (int i = 0; i < lis_discharge[ind].state.Count; i++) { lis_ste.Add(lis_discharge[ind].state[i]); }
                BsonDocument[] arr_ste = new BsonDocument[lis_ste.Count];
                for (int j = 0; j < lis_ste.Count; j++) { arr_ste[j] = lis_ste[j].ToBsonDocument(); }
                bbt_db.insert_many(host, port, database, "dt_baby_state_log", arr_ste);

                List<Bed> lis_bed = new List<Bed>();
                for (int i = 0; i < lis_discharge[ind].bed.Count; i++) { lis_bed.Add(lis_discharge[ind].bed[i]); }
                for (int i = 0; i < lis_discharge[ind].mother.bed.Count; i++) { lis_bed.Add(lis_discharge[ind].mother.bed[i]); }
                BsonDocument[] arr_bed = new BsonDocument[lis_bed.Count];
                for (int j = 0; j < lis_bed.Count; j++) { arr_bed[j] = lis_bed[j].ToBsonDocument(); }
                bbt_db.insert_many(host, port, database, "dt_bed", arr_bed);

                List<lis_ticket> lis_tic = new List<lis_ticket>();
                for (int i = 0; i < lis_discharge[ind].ticket.Count; i++) { lis_tic.Add(lis_discharge[ind].ticket[i]); }
                BsonDocument[] arr_tic = new BsonDocument[lis_tic.Count];
                for (int j = 0; j < lis_tic.Count; j++) { arr_tic[j] = lis_tic[j].ToBsonDocument(); }
                bbt_db.insert_many(host, port, database, "dt_tickets", arr_tic);

                List<lis_vertwo_log> lis_ver2log = new List<lis_vertwo_log>();
                for (int i = 0; i < lis_discharge[ind].vertwo_log.Count; i++) { lis_ver2log.Add(lis_discharge[ind].vertwo_log[i]); }
                BsonDocument[] arr_ver2 = new BsonDocument[lis_ver2log.Count];
                for (int j = 0; j < lis_ver2log.Count; j++) { arr_ver2[j] = lis_ver2log[j].ToBsonDocument(); }
                bbt_db.insert_many(host, port, database, "dt_vertwo_log", arr_ver2);

                if ((lis_discharge.Count == 1) || (ind == (lis_discharge.Count - 1)))
                {
                    bbt_db.delete_Discharge(host, port, database, tag_child);
                    res = (int)status.stt_success;
                }
            }

            return res;
        }

        private PatientDevice_Group PDG_create_obj(string sn, string hn, tag_role role, int upd_hn, int ord, int near, string near_sn, int unauth_trg, int pro_trg, int near_por_trg)
        {
            PatientDevice_Group obj_pdg = new PatientDevice_Group();
            obj_pdg.TagSerial = sn;
            obj_pdg.PatienHN = hn;
            obj_pdg.Role = role;
            obj_pdg.Update_HN = upd_hn;
            obj_pdg.Order = ord;
            obj_pdg.Near = near;
            obj_pdg.Near_Serial = near_sn;
            obj_pdg.Unauthorized_Trigger = unauth_trg;
            obj_pdg.Prohibit_Trigger = pro_trg;
            obj_pdg.Near_Portal_Trigger = near_por_trg;

            return obj_pdg;
        }

        private Baby Baby_create_obj(string hn, string hn_mom, int near_mom, baby_status sts, int ord, int upd_hn, DateTime bond_dt, DateTime dis_dt, int ste, int batt_trg)
        {
            Baby obj_baby = new Baby();
            obj_baby.HN = hn;
            obj_baby.Mother_HN = hn_mom;
            obj_baby.Near_mom = near_mom;
            obj_baby.Status = sts;
            obj_baby.Order = ord;
            obj_baby.Upd_HN = upd_hn;
            obj_baby.Bond_Date = bond_dt;
            obj_baby.Dis_Date = dis_dt;
            obj_baby.State = ste;
            obj_baby.Low_batt_trig = batt_trg;

            return obj_baby;
        }

        private tag_zone TagZone_create_obj(string sn, string hn, int ord, int near, string near_sn, int near_por_trg, string bc_sn, string zone_code, int rssi, double batt, List<string> lis_bc_1st, string bc_ty_1st, List<string> lis_code_1st, List<int> lis_rssi_1st, List<string> lis_bc_2nd, string bc_ty_2nd, List<string> lis_code_2nd, List<int> lis_rssi_2nd, int trg, DateTime ts)
        {
            tag_zone obj_tagzone = new tag_zone();
            obj_tagzone.tag_serial = sn;
            obj_tagzone.patient_hn = hn;
            obj_tagzone.order = ord;
            obj_tagzone.near = near;
            obj_tagzone.near_serial = near_sn;
            obj_tagzone.near_portal_trigger = near_por_trg;
            obj_tagzone.beacon_serial = bc_sn;
            obj_tagzone.zone_zone_code = zone_code;
            obj_tagzone.rssi = rssi;
            obj_tagzone.battery = batt;
            obj_tagzone.lis_bc_1st = lis_bc_1st;
            obj_tagzone.lis_bc_ty_1st = bc_ty_1st;
            obj_tagzone.lis_code_1st = lis_code_1st;
            obj_tagzone.lis_rssi_1st = lis_rssi_1st;
            obj_tagzone.lis_bc_2nd = lis_bc_2nd;
            obj_tagzone.lis_bc_ty_2nd = bc_ty_2nd;
            obj_tagzone.lis_code_2nd = lis_code_2nd;
            obj_tagzone.lis_rssi_2nd = lis_rssi_2nd;
            obj_tagzone.trigger = trg;
            obj_tagzone.timestamp = ts;

            return obj_tagzone;
        }

        private tag_zone_log TagZoneLog_create_obj(string sn, string bc, string zone_code, string zone_name, int trg, DateTime ts)
        {
            tag_zone_log obj = new tag_zone_log();
            obj.tag_serial = sn;
            obj.beacon_serial = bc;
            obj.zone_zone_code = zone_code;
            obj.zone_name = zone_name;
            obj.trigger_change = trg;
            obj.timestamp = ts;

            return obj;
        }
        ////////////////////////////////////////////////////////////

        public int Transition(string host, int port, string database, string hn, string order, int indx_hos, int indx_from, int indx_to)
        {
            int res = (int)status.stt_error;
            if (order == "-") { order = "1"; }
            int ord = Int32.Parse(order);
            List<PatientDevice_Group> lis_pdg = bbt_db.get_PatientDeviceGroup_hn(host, port, database, hn, ord);
            List<Baby> lis_baby = bbt_db.get_Baby(host, port, database, hn, ord);
            string from_code = cvrt_trans_Indx2Zone(indx_hos, indx_from);
            string to_code = cvrt_trans_Indx2Zone(indx_hos, indx_to);
            string from_ty = cvrt_trans_Indx2BCtype(indx_from);
            string to_ty = cvrt_trans_Indx2BCtype(indx_to);
            if (lis_pdg.Count > 0 && lis_baby.Count > 0 && from_code != "" && to_code != "")
            {
                string f_name = "ที่อื่นๆ";
                string t_name = "ที่อื่นๆ";

                if (from_ty == "R")
                {
                    List<Patient> lis_patient = bbt_db.get_Patient_by_Mom(host, port, database, lis_baby[0].Mother_HN);
                    if (lis_patient.Count > 0)
                    {
                        from_code = lis_patient[0].bed;
                        if (from_code == "753-55")
                        {
                            from_code = "753";
                        }
                        else if (from_code == "760-62")
                        {
                            from_code = "761";
                        }
                        else if (from_code == "760-62")
                        {
                            from_code = "762";
                        }
                        f_name = lis_patient[0].bed;
                        if (f_name == "753")
                        {
                            f_name = "753-55";
                        }
                        else if (f_name == "761")
                        {
                            f_name = "760-62";
                        }
                        else if (f_name == "762")
                        {
                            f_name = "760-62";
                        }

                    }
                }
                else
                {
                    List<Zone> lis_f_zone = bbt_db.get_Zone(host, port, database, from_code);
                    if (lis_f_zone.Count > 0)
                    {
                        f_name = lis_f_zone[0].Name;
                    }
                }

                if (to_ty == "R")
                {
                    List<Patient> lis_patient = bbt_db.get_Patient_by_Mom(host, port, database, lis_baby[0].Mother_HN);
                    if (lis_patient.Count > 0)
                    {
                        to_code = lis_patient[0].bed;
                        if (to_code == "753-55")
                        {
                            to_code = "753";
                        }
                        else if (to_code == "760-62")
                        {
                            to_code = "761";
                        }
                        else if (to_code == "760-62")
                        {
                            to_code = "762";
                        }
                        t_name = lis_patient[0].bed;
                        if (t_name == "753")
                        {
                            t_name = "753-55";
                        }
                        else if (t_name == "761")
                        {
                            t_name = "760-62";
                        }
                        else if (t_name == "762")
                        {
                            t_name = "760-62";
                        }

                    }
                }
                else
                {
                    List<Zone> lis_t_zone = bbt_db.get_Zone(host, port, database, to_code);
                    if (lis_t_zone.Count > 0)
                    {
                        t_name = lis_t_zone[0].Name;
                    }
                }

                DateTime cur_ts = DateTime.Now;
                DateTime stop_ts = new DateTime(1990, cur_ts.Month, cur_ts.Day);

                Transition_create_obj(host, port, database, lis_pdg[0].PatienHN, lis_pdg[0].Order, lis_pdg[0].TagSerial, from_code, f_name, from_ty, to_code, t_name, to_ty, 3, 0, 0, cur_ts, stop_ts, "ระบบ", "ระบบ", "ระบบ", (int)trans_status.active);
                bbt_db.update_Baby(host, port, database, lis_pdg[0].PatienHN, lis_pdg[0].Order, (int)baby_status.transition, (int)baby_state.authorized);
                res = (int)status.stt_success;
            }

            return res;
        }

        private string cvrt_trans_Indx2Zone(int indx_hos, int indx_trans)
        {
            string zone_code = "";
            switch (indx_hos)
            {
                case (int)hospital.py2:
                    switch (indx_trans)
                    {
                        case (int)transition_location.newborn:
                            zone_code = "NEWBORN";
                            break;
                        case (int)transition_location.nursery:
                            zone_code = "F11_NURSERY";
                            break;
                        case (int)transition_location.mom_room:
                            zone_code = "mother_room";
                            break;
                        case (int)transition_location.etc:
                            zone_code = "etc";
                            break;
                    }
                    break;
                case (int)hospital.py3:
                    switch (indx_trans)
                    {
                        case (int)transition_location.newborn:
                            zone_code = "nursery_3";
                            break;
                        case (int)transition_location.nursery:
                            zone_code = "F7_nursery";
                            break;
                        case (int)transition_location.mom_room:
                            zone_code = "mother_room";
                            break;
                        case (int)transition_location.etc:
                            zone_code = "etc";
                            break;
                    }
                    break;
                case (int)hospital.pys:
                    switch (indx_trans)
                    {
                        case (int)transition_location.newborn:
                            zone_code = "F2_NURSERY";
                            break;
                        case (int)transition_location.nursery:
                            zone_code = "F7_NURSERY";
                            break;
                        case (int)transition_location.mom_room:
                            zone_code = "mother_rrom";
                            break;
                        case (int)transition_location.etc:
                            zone_code = "etc";
                            break;
                    }
                    break;
            }

            return zone_code;
        }

        private string cvrt_trans_Indx2BCtype(int indx_trans)
        {
            string ty = "E";
            switch (indx_trans)
            {
                case (int)transition_location.newborn:
                    ty = "B";
                    break;
                case (int)transition_location.nursery:
                    ty = "N";
                    break;
                case (int)transition_location.mom_room:
                    ty = "R";
                    break;
            }

            return ty;
        }

        private void Transition_create_obj(string host, int port, string database, string hn, int ord, string sn, string f_loc, string f_loc_name, string f_loc_ty, string to_loc, string to_loc_name, string to_loc_ty, int du_hr, int du_min, int du_sec, DateTime cur_ts, DateTime stop_ts, string nurse_name, string dep, string role, int isActive)
        {
            transition obj_trans = new transition();
            obj_trans.patient_hn = hn;
            obj_trans.order = ord;
            obj_trans.tag_serial = sn;
            obj_trans.from_loc = f_loc;
            obj_trans.from_loc_name = f_loc_name;
            obj_trans.from_loc_type = f_loc_ty;
            obj_trans.to_loc = to_loc;
            obj_trans.to_loc_name = to_loc_name;
            obj_trans.to_loc_type = to_loc_ty;
            obj_trans.duration_hour = du_hr;
            obj_trans.duration_min = du_min;
            obj_trans.duration_sec = du_sec;
            obj_trans.current_time = cur_ts;
            obj_trans.stop_time = stop_ts;
            obj_trans.nurse_name = nurse_name;
            obj_trans.dep = dep;
            obj_trans.role = role;
            obj_trans.IsActive = isActive;

            bbt_db.insert_one(host, port, database, "dt_transition", obj_trans.ToBsonDocument());
        }
        ////////////////////////////////////////////////////////////

        public int Transition_edit(string host, int port, string database, string hn, string order)
        {
            int res = (int)status.stt_error;
            if (order == "-") { order = "1"; }
            int ord = Int32.Parse(order);
            List<transition> lis_trans = bbt_db.get_Transition(host, port, database, hn, ord);
            List<Baby> lis_baby = bbt_db.get_Baby(host, port, database, hn, ord);
            if (lis_trans.Count > 0 && lis_baby.Count > 0)
            {
                int ind_last = lis_trans.Count - 1;
                DateTime ts = DateTime.Now;
                DateTime ts_1990 = new DateTime(1990, ts.Month, ts.Day);
                bbt_db.update_Transition(host, port, database, hn, ord, lis_trans[ind_last].current_time, ts_1990, (int)trans_status.active);
                if (lis_baby[0].State != (int)baby_state.check_point)
                {
                    bbt_db.update_Baby(host, port, database, hn, ord, (int)baby_status.transition, (int)baby_state.authorized);
                }

                res = (int)status.stt_success;
            }

            return res;
        }
        ////////////////////////////////////////////////////////////

        public int Transition_extra_time(string host, int port, string database, string hn, string order)
        {
            int res = (int)status.stt_error;
            if (order == "-") { order = "1"; }
            int ord = Int32.Parse(order);
            List<transition> lis_trans = bbt_db.get_Transition(host, port, database, hn, ord);
            List<Baby> lis_baby = bbt_db.get_Baby(host, port, database, hn, ord);
            if (lis_trans.Count > 0 && lis_baby.Count > 0)
            {
                int ind_last = lis_trans.Count - 1;
                DateTime ts = DateTime.Now;
                DateTime start_ts = lis_trans[ind_last].current_time;
                DateTime start_ts_thai = start_ts.AddHours(7);
                TimeSpan sub_ts = ts.Subtract(start_ts_thai);
                int edit_hr = lis_trans[ind_last].duration_hour + sub_ts.Hours + 3;
                bbt_db.update_Transition(host, port, database, lis_trans[ind_last].tag_serial, start_ts, edit_hr, (int)trans_status.active);
                if (lis_baby[0].State != (int)baby_state.check_point && lis_baby[0].State != (int)baby_state.authorized)
                {
                    bbt_db.update_Baby(host, port, database, hn, ord, (int)baby_status.transition, (int)baby_state.authorized);
                }
                bbt_db.update_UIAlert_confirmd_TimeOut(host, port, database, hn, ord);

                res = (int)status.stt_success;
            }

            return res;
        }
        ////////////////////////////////////////////////////////////

        public int Transition_cancel(string host, int port, string database, string hn, string order)
        {
            int res = (int)status.stt_error;
            if (order == "-") { order = "1"; }
            int ord = Int32.Parse(order);
            List<transition> lis_trans = bbt_db.get_Transition(host, port, database, hn, ord);
            List<Baby> lis_baby = bbt_db.get_Baby(host, port, database, hn, ord);
            if (lis_trans.Count > 0 && lis_baby.Count > 0)
            {
                int ind_last = lis_trans.Count - 1;
                bbt_db.update_Transition(host, port, database, hn, ord, lis_trans[ind_last].current_time, (int)trans_status.cancel_trans);
                bbt_db.update_Baby(host, port, database, hn, ord, (int)baby_status.active, (int)baby_state.normal_no_name);

                res = (int)status.stt_success;
            }

            return res;
        }
        ////////////////////////////////////////////////////////////

        public int UIAlert_mute(string host, int port, string database, string hn, string order)
        {
            int res = (int)status.stt_error;
            if (order == "-") { order = "1"; }
            int ord = Int32.Parse(order);
            List<alert_events> lis_alert = bbt_db.get_UIAlert(host, port, database, hn, ord);
            for (int i = 0; i < lis_alert.Count; i++)
            {
                if (lis_alert[i].resolved == (int)resolve.unconfirmed)
                {
                    bbt_db.update_UIAlert_mute(host, port, database, hn, ord, lis_alert[i].timestamp);
                }
                res = (int)status.stt_success;
            }

            return res;
        }
        ////////////////////////////////////////////////////////////

        public int UIAlert_confirmd(string host, int port, string database, string hn, string order)
        {
            int res = (int)status.stt_error;
            if (order == "-") { order = "1"; }
            int ord = Int32.Parse(order);
            List<alert_events> lis_alert = bbt_db.get_UIAlert(host, port, database, hn, ord);
            for (int i = 0; i < lis_alert.Count; i++)
            {
                if (lis_alert[i].resolved != (int)resolve.confirmed)
                {
                    bbt_db.update_UIAlert_confirmd(host, port, database, hn, ord, lis_alert[i].timestamp);
                }
                res = (int)status.stt_success;
            }

            return res;
        }
        ////////////////////////////////////////////////////////////

        public int Tag_lost(string host, int port, string database, string hn, string order)
        {
            int res = (int)status.stt_error;
            if (order == "-") { order = "1"; }
            int ord = Int32.Parse(order);
            List<Baby> lis_baby = bbt_db.get_Baby(host, port, database, hn, ord);
            List<alert_events> lis_alert = bbt_db.get_UIAlert(host, port, database, hn, ord);
            List<transition> lis_trans = bbt_db.get_Transition(host, port, database, hn, ord);
            if (lis_baby.Count > 0)
            {
                int ind_last = lis_trans.Count - 1;
                if (lis_trans[ind_last].IsActive != (int)trans_status.completed && lis_trans[ind_last].IsActive != (int)trans_status.cancel_trans)
                {
                    if (lis_trans[ind_last].from_loc_type == "E" || lis_trans[ind_last].to_loc_type == "E")
                    {
                        bbt_db.update_Baby_status(host, port, database, hn, ord, (int)baby_status.stealth);
                    }
                    else
                    {
                        bbt_db.update_Baby_status(host, port, database, hn, ord, (int)baby_status.transition);
                    }

                }
                else
                {
                    bbt_db.update_Baby_status(host, port, database, hn, ord, (int)baby_status.active);
                }

                for (int i = 0; i < lis_alert.Count; i++)
                {
                    if (lis_alert[i].event_type == (int)event_type.strap_cut || lis_alert[i].event_type == (int)event_type.tag_lost || lis_alert[i].event_type == (int)event_type.soft_signal)
                    {
                        bbt_db.update_UIAlert_confirmd(host, port, database, hn, ord, lis_alert[i].timestamp);
                    }
                    res = (int)status.stt_success;
                }
            }

            return res;
        }
        ////////////////////////////////////////////////////////////

        public int Tag_login(string host, int port, string database, string hn, string order)
        {
            int res = (int)status.stt_error;
            if (order == "-") { order = "1"; }
            int ord = Int32.Parse(order);
            bbt_db.update_Baby_state(host, port, database, hn, ord, (int)baby_state.normal_no_name);
            res = (int)status.stt_success;

            return res;
        }
        ////////////////////////////////////////////////////////////
        
        public int Edit_serial_tag(string host, int port, string database, string sn_from, string sn_change)
        {
            int res = (int)status.stt_error;
            bbt_db.update_Tag_editSN(host, port, database, sn_from, sn_change);
            bbt_db.update_PDG_editSN(host, port, database, sn_from, sn_change);
            bbt_db.update_TagZone_editSN(host, port, database, sn_from, sn_change);
            bbt_db.update_TagZoneLog_editSN(host, port, database, sn_from, sn_change);
            bbt_db.update_TagZoneLogTemp_editSN(host, port, database, sn_from, sn_change);
            bbt_db.update_Transition_editSN(host, port, database, sn_from, sn_change);
            bbt_db.update_UIAlert_editSN(host, port, database, sn_from, sn_change);
            bbt_db.update_AlertLog_editSN(host, port, database, sn_from, sn_change);
            res = (int)status.stt_success;
            return res;
        }
        ////////////////////////////////////////////////////////////
        
        public int Edit_HN_child(string host, int port, string database, string hn_from, string ord_from, string hn_change, string ord_change)
        {
            int res = (int)status.stt_error;

            if (ord_from == "-") { ord_from = "1"; }
            int ord_f = Int32.Parse(ord_from);
            if (ord_change == "-") { ord_change = "1"; }
            int ord_c = Int32.Parse(ord_change);

            bbt_db.update_Patient_editHN(host, port, database, hn_from, ord_f, hn_change, ord_c);
            bbt_db.update_PDG_editHN(host, port, database, hn_from, ord_f, hn_change, ord_c);
            bbt_db.update_Baby_editHN(host, port, database, hn_from, ord_f, hn_change, ord_c);
            bbt_db.update_TagZone_editHN(host, port, database, hn_from, ord_f, hn_change, ord_c);
            bbt_db.update_Transition_editHN(host, port, database, hn_from, ord_f, hn_change, ord_c);
            bbt_db.update_TagZoneLog_editHN(host, port, database, hn_from, ord_f, hn_change, ord_c);
            bbt_db.update_BabyStateLog_editHN(host, port, database, hn_from, ord_f, hn_change, ord_c);
            bbt_db.update_Bed_editHN(host, port, database, hn_from, ord_f, hn_change, ord_c);
            bbt_db.update_UIAlert_editHN(host, port, database, hn_from, ord_f, hn_change, ord_c);
            bbt_db.update_UILog_editHN(host, port, database, hn_from, ord_f, hn_change, ord_c);
            bbt_db.update_UIAlertLog_editHN(host, port, database, hn_from, ord_f, hn_change, ord_c);

            res = (int)status.stt_success;
            return res;
        }

        public int Room_edit(string host, int port, string database, string hn, string order, string room, string fl)
        {
            int res = (int)status.stt_error;
            int role = (int)tag_role.child;
            if (order == "-") { order = "1"; }
            int ord = Int32.Parse(order);
            bbt_db.update_Patient_bed(host, port, database, hn, ord, room);
            if (ord == 0) role = (int)tag_role.mother;
            Bed bed_obj = Bed_create_obj(host, port, database, DateTime.Now, hn, room, role, ord, Int32.Parse(fl));
            res = (int)status.stt_success;

            return res;
        }

        public int Room_out_edit(string host, int port, string database, string hn, string order)
        {
            int res = (int)status.stt_error;
            if (order == "-") { order = "1"; }
            int ord = Int32.Parse(order);
            bbt_db.update_Patient_bed(host, port, database, hn, ord);
            Bed bed_obj = Bed_create_obj(host, port, database, DateTime.Now, hn, "dis", (int)tag_role.mother, ord, -1);
            res = (int)status.stt_success;

            return res;
        }

        private Bed Bed_create_obj(string host, int port, string database, DateTime ts, string hn, string bed, int role, int ord, int fl)
        {
            Bed obj = new Bed();
            obj.timestamp = ts;
            obj.patient_hn = hn;
            obj.bed = bed;
            obj.role = role;
            obj.order = ord;
            obj.floor = fl;

            bbt_db.insert_one(host, port, database, "dt_bed", obj.ToBsonDocument());
            return obj;
        }
        ////////////////////////////////////////////////////////////
        
        public int Clear_DB(string host, int port, string database)
        {
            int res = (int)status.stt_error;
            res = (int)status.stt_success;
            bbt_db.delete_Unnecessary_data(host, port, database, "dt_card_log");
            bbt_db.delete_Unnecessary_data(host, port, database, "dt_tag_zone_logs_temp");
            bbt_db.delete_Unnecessary_data(host, port, database, "dt_transition_temp");
            bbt_db.delete_Unnecessary_data(host, port, database, "dt_vertwo_log");
            bbt_db.delete_Unnecessary_data(host, port, database, "it_command_beacons");
            bbt_db.delete_Unnecessary_data(host, port, database, "st_systemlogs");
            bbt_db.delete_Unnecessary_data(host, port, database, "test");
            bbt_db.delete_Unnecessary_data(host, port, database, "test_alert");
            bbt_db.delete_Unnecessary_data(host, port, database, "ui_alerts_beacon");
            return res;
        }

        public List<beacon_location> Get_list_beacon(string host, int port, string database, string type, string fl)
        {
            List<beacon_location> list_bc = new List<beacon_location>();
            if (fl == "All")
            {
                list_bc = bbt_db.get_BeaconLocation_AllFl(host, port, database, type);
            }
            else
            {
                list_bc = bbt_db.get_BeaconLocation(host, port, database, type, Int32.Parse(fl));
            }

            return list_bc;
        }

        public void Beacon_command_set(string host, int port, string database, string bc, string bc_type, int cnt, int type, int inst)
        {
            switch (type)
            {
                case 1:
                    Beacon_command_format(host, port, database, bc, bc_type, cnt, 0, "blink", "red", 100, "repeat", 100, 100, inst);
                    break;
                case 2:
                    Beacon_command_format(host, port, database, bc, bc_type, cnt, 0, "off", "red", 100, "off", 100, 100, inst);
                    break;
                case 3:
                    Beacon_command_format(host, port, database, bc, bc_type, cnt, 0, "blink", "red", 100, "off", 100, 100, inst);
                    break;
                case 4:
                    Beacon_command_format(host, port, database, bc, bc_type, cnt, 0, "on", "red", 100, "off", 100, 100, inst);
                    break;
                case 5:
                    Beacon_command_format(host, port, database, bc, bc_type, cnt, 0, "on", "green", 100, "once", 1000, 1000, inst);
                    break;
                case 6:
                    Beacon_command_format(host, port, database, bc, bc_type, cnt, 0, "on", "red", 100, "once", 50, 50, inst);
                    break;
                case 7:
                    Beacon_command_format(host, port, database, bc, bc_type, cnt, 0, "on", "red", 100, "once", 500, 500, inst);
                    break;
                case 8:
                    Beacon_command_format(host, port, database, bc, bc_type, cnt, 0, "on", "green", 100, "off", 500, 500, inst);
                    break;
                case 9:
                    Beacon_command_format(host, port, database, bc, bc_type, cnt, 0, "on", "red", 100, "off", 500, 500, inst);
                    break;
                default:
                    break;
            }
        }

        private void Beacon_command_format(string host, int port, string database, string bc, string bc_type, int ct, int du, string lm, string lc, int lp, string sm, int sn, int sf, int inst)
        {
            DateTime datetime = new DateTime();
            string ts = cvrt_datetime2string(datetime, 1);
            cont_obj cont_Obj = new cont_obj();
            cont_Obj.dur = 0;
            cont_Obj.lmode = lm;
            cont_Obj.lcolor = lc;
            cont_Obj.lperiod = lp;
            cont_Obj.smode = sm;
            cont_Obj.sontime = sn;
            cont_Obj.sofftime = sf;

            proto_obj proto_Obj = new proto_obj();
            proto_Obj.ver = "1.0.0";
            proto_Obj.ts = ts;
            proto_Obj.mthd = (int)method.set;
            proto_Obj.tpc = (int)topic.command;
            proto_Obj.cmd = (int)command.alarm;
            proto_Obj.cont = cont_Obj;

            command_beacon obj = new command_beacon();
            obj.beacon_serial = bc;
            obj.beacon_type = bc_type;
            obj.cnt = ct;
            obj.proto = proto_Obj;

            if (inst == 1)
            {
                List<command_beacon> cmd_beacon = bbt_db.get_CommandBC(host, port, database, bc_type, bc);
                if (cmd_beacon.Count < 1)
                {
                    bbt_db.update_Beacon(host, port, database, bc, ct);
                    bbt_db.insert_one(host, port, database, "it_command_beacons", obj.ToBsonDocument());
                }
            }
        }

        private string cvrt_datetime2string(DateTime dt, int type)
        {
            int year = dt.Year;
            string month = cvrt_time_int2string((dt.Month + 1), 2);
            string day = cvrt_time_int2string(dt.Day, 2);
            string hour = cvrt_time_int2string(dt.Hour, 2);
            string mi = cvrt_time_int2string(dt.Minute, 2);
            string sec = cvrt_time_int2string(dt.Second, 2);
            string msec = cvrt_time_int2string(dt.Millisecond, 3);

            string ts = year + month + day + "_" + hour + mi + sec + msec;
            return ts;
        }

        private string cvrt_time_int2string(int time_int, int pos)
        {
            if ((time_int < 100) && (time_int > -1))
            {
                string time_str = "";

                if (time_int < 10)
                {
                    if (pos == 2)
                    {
                        time_str = "0" + time_int;
                    }
                    else
                    {
                        time_str = "00" + time_int;
                    }
                }
                else
                {
                    if (pos == 2)
                    {
                        time_str = time_int.ToString();
                    }
                    else
                    {
                        time_str = "0" + time_int;
                    }
                }
                return time_str;
            }
            else
            {
                return time_int.ToString();
            }
        }
        ////////////////////////////////////////////////////////////

        public List<beacon_location> Get_all_list_beacon_location(string host, int port, string database)
        {
            List<beacon_location> list_bc = new List<beacon_location>();
            list_bc = bbt_db.get_BeaconLocation_All(host, port, database);
            return list_bc;
        }

        public List<Data> Get_Data(string host, int port, string database)
        {
            List<Data> res = new List<Data>();
            res = bbt_db.get_Data(host, port, database);
            return res;
        }

        public List<string> Sort_select_bc_or_tag(List<Data> res, int rssi_selt)
        {
            List<string> ret = new List<string>();
            if (rssi_selt == (int)rssi_select.beacon)
            {
                for (int i = 0; i < res.Count; i++)
                {
                    ret.Add(res[i].Source.Serial);
                }
            }
            else if (rssi_selt == (int)rssi_select.tag)
            {
                for (int i = 0; i < res.Count; i++)
                {
                    for (int j = 0; j < res[i].Contents.Count; j++)
                    {
                        ret.Add(res[i].Contents[j].sn);
                    }
                }
            }
            ret.Sort();
            ret = ret.Distinct().ToList();
            return ret;
        }

        public List<RSSI_checking> RSSI_sort_by(List<Data> list_bc, List<beacon_location> list_bc_loc, int rssi_selt, string rssi_selt_str, int rssi_sort)
        {
            List<RSSI_checking> ret = new List<RSSI_checking>();
            string code_str = "";
            if(rssi_selt == (int)rssi_select.beacon)
            {
                for(int i=0; i<list_bc.Count; i++)
                {
                    string bc = list_bc[i].Source.Serial.ToUpper();
                    if (list_bc[i].Source.Serial == rssi_selt_str)
                    {
                        for(int j=0; j<list_bc[i].Contents.Count; j++)
                        {
                            for (int m = 0; m < list_bc_loc.Count; m++)
                            {
                                if (list_bc_loc[m].beacon_serial == bc)
                                {
                                    code_str = list_bc_loc[m].code;
                                }
                            }

                            RSSI_checking obj = new RSSI_checking();
                            obj.bc_serial = bc;
                            obj.tag_serial = list_bc[i].Contents[j].sn;
                            obj.rssi = cvrt_unsigned2signed(list_bc[i].Contents[j].rssi);
                            obj.code = code_str;
                            obj.evnt = (int)list_bc[i].Contents[j].evnt;
                            ret.Add(obj);
                        }
                    }
                }
            }
            else if(rssi_selt == (int)rssi_select.tag)
            {
                for(int i=0; i< list_bc.Count; i++)
                {
                    for(int j=0; j< list_bc[i].Contents.Count; j++)
                    {
                        if(list_bc[i].Contents[j].sn == rssi_selt_str)
                        {
                            string bc = list_bc[i].Source.Serial.ToUpper();
                            for (int m=0; m< list_bc_loc.Count; m++)
                            {
                                if(list_bc_loc[m].beacon_serial == bc)
                                {
                                    code_str = list_bc_loc[m].code;
                                }
                            }

                            RSSI_checking obj = new RSSI_checking();
                            obj.bc_serial = bc;
                            obj.tag_serial = list_bc[i].Contents[j].sn;
                            obj.rssi = cvrt_unsigned2signed(list_bc[i].Contents[j].rssi);
                            obj.code = code_str;
                            obj.evnt = (int)list_bc[i].Contents[j].evnt;
                            ret.Add(obj);
                        }
                    }
                }
            }

            if(rssi_sort == (int)rssi_sort_by.beacon)
            {
                ret = ret.OrderBy(x => x.bc_serial).ToList();
            }
            else if (rssi_sort == (int)rssi_sort_by.tag)
            {
                ret = ret.OrderBy(x => x.tag_serial).ToList();
            }
            else if (rssi_sort == (int)rssi_sort_by.rssi)
            {
                ret = ret.OrderByDescending(x => x.rssi).ToList();
            }
            else if (rssi_sort == (int)rssi_sort_by.code)
            {
                ret = ret.OrderBy(x => x.code).ToList();
            }

            return ret;
        }

        private int cvrt_unsigned2signed(int unsign)
        {
            int sign_tmp = 0;
            int sign = 0;
            sign_tmp = unsign - 128;
            sign = (-128) + sign_tmp;
            return sign;
        }
    }
}
