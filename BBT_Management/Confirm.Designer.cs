﻿namespace BBT_Management
{
    partial class ConfirmForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lb_conf = new System.Windows.Forms.Label();
            this.bt_conf_OK = new System.Windows.Forms.Button();
            this.bt_conf_Cancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lb_conf
            // 
            this.lb_conf.AutoSize = true;
            this.lb_conf.Location = new System.Drawing.Point(28, 19);
            this.lb_conf.Name = "lb_conf";
            this.lb_conf.Size = new System.Drawing.Size(13, 13);
            this.lb_conf.TabIndex = 0;
            this.lb_conf.Text = "?";
            // 
            // bt_conf_OK
            // 
            this.bt_conf_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bt_conf_OK.Location = new System.Drawing.Point(31, 101);
            this.bt_conf_OK.Name = "bt_conf_OK";
            this.bt_conf_OK.Size = new System.Drawing.Size(75, 23);
            this.bt_conf_OK.TabIndex = 1;
            this.bt_conf_OK.Text = "OK";
            this.bt_conf_OK.UseVisualStyleBackColor = true;
            // 
            // bt_conf_Cancel
            // 
            this.bt_conf_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bt_conf_Cancel.Location = new System.Drawing.Point(125, 101);
            this.bt_conf_Cancel.Name = "bt_conf_Cancel";
            this.bt_conf_Cancel.Size = new System.Drawing.Size(75, 23);
            this.bt_conf_Cancel.TabIndex = 2;
            this.bt_conf_Cancel.Text = "Cancel";
            this.bt_conf_Cancel.UseVisualStyleBackColor = true;
            // 
            // ConfirmForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(230, 136);
            this.Controls.Add(this.bt_conf_Cancel);
            this.Controls.Add(this.bt_conf_OK);
            this.Controls.Add(this.lb_conf);
            this.Name = "ConfirmForm";
            this.Text = "Confirm";
            this.Load += new System.EventHandler(this.ConfirmForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lb_conf;
        private System.Windows.Forms.Button bt_conf_OK;
        private System.Windows.Forms.Button bt_conf_Cancel;
    }
}