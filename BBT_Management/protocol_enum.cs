﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBT_Management
{
    // Use
    public enum method
    {
        undefined_mthd  = -1,
        upload          = 1,
        request         = 2,
        set             = 3,
        get             = 4
    };
    // Use
    public enum topic
    {
        undefined_tpc   = -1,
        data            = 0,
        eventActivity   = 1,
        pair            = 2,
        status          = 3,
        command         = 4,
        setting         = 5,
        log             = 6,
        get_hos         = 41,
        get_child       = 42,
        get_room        = 43,
        set_beacon      = 50,
        set_location    = 51,
        transition      = 52,
        tran_cancel     = 53,
        discharge       = 54,
        set_child       = 55,
        mute_alert      = 58,
        cancel_alert    = 59
    };
    public enum scc
    {
        server      = 0,
        beacon      = 1,
        bonder      = 2,
        station     = 3,
        child_tag   = 11,
        mother_tag  = 12
    };
    public enum communication_status
    {
        success     = 0,
        error       = 1
    };
    public enum network_stastus
    {
        success     = 0,
        error       = 1
    };
    public enum status_pair
    {
        disable     = 0,
        active      = 1
    };
    // Use
    public enum event_type
    {
        normal              = 0,
        strap_cut           = 1,
        tag_lost            = 2,
        soft_signal         = 3,
        unauthorized_card   = 4,
        portal              = 5,
        time_out            = 6,
        low_battery         = 7,
        unware              = 8,
        beacon_lost         = 9,
        beacon_no_resp      = 10,
        gate_lost           = 11,
        prohibit            = 12,
        discharged          = 13,
        db_hos              = 14,
        unauthorized        = 15,
        near_by_mom         = 128
    };
    // Use
    public enum command
    {
        factory_reset   = 0,
        reset           = 1,
        alarm           = 2
    };
    public enum error
    {
        syntax_error            = 1,
        incompatible_version    = 2,
        source_not_found        = 3,
        invalid_attribute       = 4,
        invald_value_type       = 5,
        unspecified             = 99
    };
    public enum timestamp
    {
        full        = 18,
        miss_ms     = 15,
        miss_sec    = 13,
        miss_min    = 12
    };
    // Use
    public enum hospital
    {
        py2     = 0,
        py3     = 1,
        pys     = 2,
        demo1   = 3,
        demo2   = 4
    };
    // Use
    public enum transition_location
    {
        newborn     = 0,
        nursery     = 1,
        mom_room    = 2,
        etc         = 3
    };
    // Use
    public enum baby_status
    {
        fairy       = 0,
        active      = 1,
        no_signal   = 2,
        with_mom    = 128,
        transition  = 11,
        discharge   = 12,
        stealth     = 13
    };
    public enum gender
    {
        female  = 0,
        male    = 1
    };
    // Use
    public enum tag_role
    {
        mother  = 0,
        child   = 1
    };
    public enum status_pairing
    {
        disable     = 0,
        active      = 1
    };
    public enum LogEventCode
    {
        //Server Section
        server_stopped              = 0,
        server_started              = 1,
        server_restarted            = 2,
        server_connection_error     = 3,

        //Client Section
        client_disconnected         = 10,
        client_connected            = 11,
        client_missing              = 12,
        client_connection_error     = 13,

        //Protocol Section
        protocol_syntax_error       = 20

        //Database Section
    };
    public enum UserRoleType
    {
        None            = 0,
        User            = 1,
        Administrator   = 2,
        Above           = 3
    };
    // Use
    public enum status
    {
        stt_success = 0,
        stt_error   = 1
    };
    // Use
    public enum baby_state
    {
        new_pair                    = 0,
        destroy                     = -1,
        newborn                     = 1,
        to_observe_transfer         = 2,
        observe                     = 3,
        to_nursery_transfer         = 4,
        wait_card_auth              = 5,
        authorized                  = 6,
        check_point                 = 7,
        normal_no_name              = 8,
        normal_with_name            = 9,
        no_mom_diff_floor_transfer  = 10,
        no_mom_same_floor_transfer  = 11,
        diff_floor_transfer         = 12,
        same_floor_transfer         = 13,
        discharged                  = 14
    };
    public enum correct_scid
    {
        incorr  = 0,
        corr    = 1
    };
    public enum wena
    {
        disable     = 0,
        enable      = 1
    };
    public enum wsnd 
    {
        off         = 0,
        non_wtch    = 1,
        wtch        = 2,
        anyone      = 3
    };
    public enum wled
    {
        off             = 0,
        all             = 1,
        red_non_wtch    = 2,
        green_wtch      = 3
    };
    // Use
    public enum resolve
    {
        unconfirmed = 1,
        confirmed   = 2,
        mute        = 0
    };
    public enum baby_near
    {
        normal      = 0,
        gate        = 1,
        card        = 2,
        prohibit    = 3,
        portal      = 4,
        portal_w    = 5,
        portal_ob   = 6,
        portal_etc  = 7,
        hallway     = 8
    };
    public enum baby_upd
    {
        updated     = 1,
        unupdate    = 0,
        update_hn   = 2
    };
    // Use
    public enum trans_status
    {
        waiting         = 0,
        active          = 1,
        completed       = 11,
        time_out        = 12,
        stealth         = 13,
        cancel_trans    = 14 
    };
    public enum event_device_type
    {
        tag = 1,
        bc  = 2,
        db  = 3
    };
    public enum beacon_reset
    {
        normal  = 0,
        reset   = 1
    };

    // Use
    public enum rssi_select
    {
        beacon = 1,
        tag = 2
    }

    // Use
    public enum rssi_sort_by
    {
        beacon = 1,
        tag = 2,
        rssi = 3,
        code = 4
    }
}
