﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Core;
using System.Threading;

namespace BBT_Management
{
    internal class bbt_DB
    {
        #region Fields
        private string _host = "localhost";
        private int _port = 8085;
        private string _databastName = "bbt_DB";
        private IMongoDatabase db;                                                  // represent a database
        #endregion

        #region Constructors
        /// <summary>
        /// Database Connection Object Constructor.
        /// </summary>
        public bbt_DB()
        { }

        /// <summary>
        /// Database Connection Object Constructor.
        /// </summary>
        /// <param name="host">Host name or IP Address</param>
        /// <param name="port">TCP/IP connection Port (default : 27017)</param>
        /// <param name="database">Database Name</param>
        public bbt_DB(string host, int port, string database)
        {
            _host = host;
            _port = port;
            _databastName = database;
        }
        #endregion

        #region Database Connection
        /// <summary>
        /// Mongo Database Connection method.
        /// </summary>
        public bool connectDB()
        {
            MongoClientSettings db_setting = new MongoClientSettings();
            db_setting.Server = new MongoServerAddress(_host, _port);
            var client = new MongoClient(db_setting);
            //int count = 0;
            if (client.Cluster.Description.State.ToString() == "Disconnected")
            {
                return false;
                //Thread.Sleep(1000);
                //if (count++ >= 10)
                //{
                //    MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("ไม่สามารถเชื่อมต่อกับฐานข้อมูล โปรดติดต่อผู้ดูแลระบบเพื่อแก้ไข", "Baby Tag System", System.Windows.MessageBoxButton.OK, MessageBoxImage.Warning);
                //    Environment.Exit(0);
                //}
            }
            else
            {
                db = client.GetDatabase(_databastName);
                return true;
            }

        }

        /// <summary>
        /// Mongo Database Connection method.
        /// </summary>
        /// <param name="host">Host name or IP Address</param>
        /// <param name="port">TCP/IP connection Port (default : 27017)</param>
        public bool connectDB(string host, int port, string database)
        {
            MongoClientSettings db_setting = new MongoClientSettings();
            db_setting.Server = new MongoServerAddress(host, port);
            var client = new MongoClient(db_setting);
            var databases = client.ListDatabases();

            if (client.Cluster.Description.State.ToString() == "Disconnected")
            {
                return false;
            }
            else
            {

                db = client.GetDatabase(database);
                return true;
            }

        }

        /// <summary>
        /// Mongo Database Connection method.
        /// </summary>
        /// <param name="timeoutSec">Database Connection Timeout in Second</param>
        public bool connectDB(int timeoutSec)
        {
            MongoClientSettings db_setting = new MongoClientSettings();
            db_setting.Server = new MongoServerAddress(_host, _port);
            db_setting.ConnectTimeout = new TimeSpan(0, 0, timeoutSec);
            var client = new MongoClient(db_setting);
            if (client.Cluster.Description.State.ToString() == "Disconnected")
            {
                return false;
            }
            else
            {

                db = client.GetDatabase(_databastName);
                return true;
            }


        }
        #endregion

        #region Database Management

        internal bool IsOK(int timeout_sec)
        {
            try
            {
                MongoClientSettings db_setting = new MongoClientSettings();
                db_setting.Server = new MongoServerAddress(_host, _port);
                db_setting.ConnectTimeout = new TimeSpan(0, 0, timeout_sec);
                var client = new MongoClient(db_setting);
                db = client.GetDatabase(_databastName);

                var result = db.RunCommandAsync((Command<BsonDocument>)"{ping:1}").Wait(10000);

                return result;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        
        #endregion

        // Use
        public List<Bed> get_Bed(string host, int port, string database, string hn, int ord)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<Bed>("dt_bed");
                var result = collect.Find(x => (x.patient_hn == hn) && (x.order == ord)).ToList();
                return result;
            }
            else return null;
        }

        #region Export Data Method
        /// <summary>
        /// Query UI_Display object with specific floor
        /// </summary>
        /// <param name="fl">specific floor</param>
        /// <returns></returns>

        // Use
        public void update_Baby(string host, int port, string database, string hn, int ord, int sts, int ste)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<Baby>("dt_baby");
                var filter = Builders<Baby>.Filter.Eq(c => c.HN, hn) & Builders<Baby>.Filter.Eq(c => c.Order, ord);
                var update = Builders<Baby>.Update.Set("status", sts).Set("state", ste);
                var result = collect.UpdateOneAsync(filter, update);
            }
        }

        // Use
        public void update_Baby_status(string host, int port, string database, string hn, int ord, int sts)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<Baby>("dt_baby");
                var filter = Builders<Baby>.Filter.Eq(c => c.HN, hn) & Builders<Baby>.Filter.Eq(c => c.Order, ord);
                var update = Builders<Baby>.Update.Set("status", sts);
                var result = collect.UpdateOneAsync(filter, update);
            }
        }

        // Use
        public void update_Baby_state(string host, int port, string database, string hn, int ord, int ste)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<Baby>("dt_baby");
                var filter = Builders<Baby>.Filter.Eq(c => c.HN, hn) & Builders<Baby>.Filter.Eq(c => c.Order, ord);
                var update = Builders<Baby>.Update.Set("state", ste);
                var result = collect.UpdateOneAsync(filter, update);
            }
        }

        // Use
        public void update_Beacon(string host, int port, string database, string bc, int ct)
        {
            if (connectDB(host, port, database))
            {
                DateTime ts = new DateTime();
                var collect = db.GetCollection<beacon>("it_beacons");
                var filter = Builders<beacon>.Filter.Eq(c => c.serial, bc);
                var update = Builders<beacon>.Update.Set("resp", ct).Set("lastResp", ts);
                var result = collect.UpdateOneAsync(filter, update);
            }
        }

        // Use
        public void update_Patient_bed(string host, int port, string database, string hn, int ord)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<Patient>("dt_patient");
                var filter = Builders<Patient>.Filter.Eq(c => c.hn, hn) & Builders<Patient>.Filter.Eq(c => c.order, ord);
                var update = Builders<Patient>.Update.Set("bed", "dis");
                var result = collect.UpdateOneAsync(filter, update);
            }
        }

        // Use
        public void update_Patient_bed(string host, int port, string database, string hn, int ord, string room)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<Patient>("dt_patient");
                var filter = Builders<Patient>.Filter.Eq(c => c.hn, hn) & Builders<Patient>.Filter.Eq(c => c.order, ord);
                var update = Builders<Patient>.Update.Set("bed", room);
                var result = collect.UpdateOneAsync(filter, update);
            }
        }

        // Use
        public void update_Transition(string host, int port, string database, string sn, DateTime cur_ts, int du_hr, int trans_sts)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<transition>("dt_transition");
                var filter = Builders<transition>.Filter.Eq(c => c.tag_serial, sn) & Builders<transition>.Filter.Eq(c => c.current_time, cur_ts);
                var update = Builders<transition>.Update.Set("duration_hour", du_hr).Set("IsActive", trans_sts);
                var result = collect.UpdateOneAsync(filter, update);
            }
        }

        // Use
        public void update_Transition(string host, int port, string database, string hn, int ord, DateTime cur_ts, DateTime stop_ts, int trans_sts)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<transition>("dt_transition");
                var filter = Builders<transition>.Filter.Eq(c => c.patient_hn, hn) & Builders<transition>.Filter.Eq(c => c.order, ord) & Builders<transition>.Filter.Eq(c => c.current_time, cur_ts);
                var update = Builders<transition>.Update.Set("stop_time", stop_ts).Set("IsActive", trans_sts);
                var result = collect.UpdateOneAsync(filter, update);
            }
        }

        // Use
        public void update_Transition(string host, int port, string database, string hn, int ord, DateTime cur_ts, int trans_sts)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<transition>("dt_transition");
                var filter = Builders<transition>.Filter.Eq(c => c.patient_hn, hn) & Builders<transition>.Filter.Eq(c => c.order, ord) & Builders<transition>.Filter.Eq(c => c.current_time, cur_ts);
                var update = Builders<transition>.Update.Set("IsActive", trans_sts);
                var result = collect.UpdateOneAsync(filter, update);
            }
        }

        // Use
        public void update_UIAlert_confirmd_TimeOut(string host, int port, string database, string hn, int ord)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<alert_events>("ui_alerts");
                var filter = Builders<alert_events>.Filter.Eq(c => c.hn, hn) & Builders<alert_events>.Filter.Eq(c => c.order, ord) & Builders<alert_events>.Filter.Eq(c => c.event_type, (int)event_type.time_out);
                var update = Builders<alert_events>.Update.Set("resolved", (int)resolve.confirmed);
                var result = collect.UpdateOneAsync(filter, update);
            }
        }

        // Use
        public void update_UIAlert_mute(string host, int port, string database, string hn, int ord, DateTime ts)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<alert_events>("ui_alerts");
                var filter = Builders<alert_events>.Filter.Eq(c => c.hn, hn) & Builders<alert_events>.Filter.Eq(c => c.order, ord) & Builders<alert_events>.Filter.Eq(c => c.timestamp, ts);
                var update = Builders<alert_events>.Update.Set("resolved", (int)resolve.mute);
                var result = collect.UpdateOneAsync(filter, update);
            }
        }

        // Use
        public void update_UIAlert_confirmd(string host, int port, string database, string hn, int ord, DateTime ts)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<alert_events>("ui_alerts");
                var filter = Builders<alert_events>.Filter.Eq(c => c.hn, hn) & Builders<alert_events>.Filter.Eq(c => c.order, ord) & Builders<alert_events>.Filter.Eq(c => c.timestamp, ts);
                var update = Builders<alert_events>.Update.Set("resolved", (int)resolve.confirmed);
                var result = collect.UpdateOneAsync(filter, update);
            }
        }

        // Use
        public void update_Tag_editSN(string host, int port, string database, string sn_from, string sn_change)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<Tag>("dt_tags");
                var filter = Builders<Tag>.Filter.Eq(c => c.Serial, sn_from);
                var update = Builders<Tag>.Update.Set("serial", sn_change);
                var result = collect.UpdateOneAsync(filter, update);
            }
        }

        // Use
        public void update_PDG_editSN(string host, int port, string database, string sn_from, string sn_change)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<PatientDevice_Group>("dt_patient_device_groups");
                var filter = Builders<PatientDevice_Group>.Filter.Eq(c => c.TagSerial, sn_from);
                var update = Builders<PatientDevice_Group>.Update.Set("tag_serial", sn_change);
                var result = collect.UpdateOneAsync(filter, update);
            }
        }

        // Use
        public void update_TagZone_editSN(string host, int port, string database, string sn_from, string sn_change)
        {
            if (connectDB(host, port, database))
            {  
                var collect = db.GetCollection<tag_zone>("dt_tag_zone");
                var filter = Builders<tag_zone>.Filter.Eq(c => c.tag_serial, sn_from);
                var update = Builders<tag_zone>.Update.Set("tag_serial", sn_change);
                var result = collect.UpdateOneAsync(filter, update);
            }
        }

        // Use
        public void update_TagZoneLog_editSN(string host, int port, string database, string sn_from, string sn_change)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<tag_zone>("dt_tag_zone_logs");
                var filter = Builders<tag_zone>.Filter.Eq(c => c.tag_serial, sn_from);
                var update = Builders<tag_zone>.Update.Set("tag_serial", sn_change);
                var result = collect.UpdateManyAsync(filter, update);
            }
        }

        // Use
        public void update_TagZoneLogTemp_editSN(string host, int port, string database, string sn_from, string sn_change)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<tag_zone_log>("dt_tag_zone_logs_temp");
                var filter = Builders<tag_zone_log>.Filter.Eq(c => c.tag_serial, sn_from);
                var update = Builders<tag_zone_log>.Update.Set("tag_serial", sn_change);
                var result = collect.UpdateManyAsync(filter, update);
            }
        }

        // Use
        public void update_Transition_editSN(string host, int port, string database, string sn_from, string sn_change)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<transition>("dt_transition");
                var filter = Builders<transition>.Filter.Eq(c => c.tag_serial, sn_from);
                var update = Builders<transition>.Update.Set("tag_serial", sn_change);
                var result = collect.UpdateManyAsync(filter, update);
            }
        }

        // Use
        public void update_UIAlert_editSN(string host, int port, string database, string sn_from, string sn_change)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<alert_events>("ui_alerts");
                var filter = Builders<alert_events>.Filter.Eq(c => c.serial, sn_from);
                var update = Builders<alert_events>.Update.Set("serial", sn_change);
                var result = collect.UpdateManyAsync(filter, update);
            }
        }

        // Use
        public void update_AlertLog_editSN(string host, int port, string database, string sn_from, string sn_change)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<alert_log>("dt_alert_log");
                var filter = Builders<alert_log>.Filter.Eq(c => c.serial, sn_from);
                var update = Builders<alert_log>.Update.Set("tag_serial", sn_change);
                var result = collect.UpdateManyAsync(filter, update);
            }
        }

        // Use
        public void update_Patient_editHN(string host, int port, string database, string hn_from, int ord_from, string hn_change, int ord_change)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<Patient>("dt_patient");
                var filter = Builders<Patient>.Filter.Eq(c => c.hn, hn_from) & Builders<Patient>.Filter.Eq(c => c.order, ord_from);
                var update = Builders<Patient>.Update.Set("hn", hn_change).Set("order", ord_change);
                var result = collect.UpdateOneAsync(filter, update);
            }
        }

        // Use
        public void update_PDG_editHN(string host, int port, string database, string hn_from, int ord_from, string hn_change, int ord_change)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<PatientDevice_Group>("dt_patient_device_groups");
                var filter = Builders<PatientDevice_Group>.Filter.Eq(c => c.PatienHN, hn_from) & Builders<PatientDevice_Group>.Filter.Eq(c => c.Order, ord_from);
                var update = Builders<PatientDevice_Group>.Update.Set("patient_hn", hn_change).Set("order", ord_change);
                var result = collect.UpdateOneAsync(filter, update);
            }
        }

        // Use
        public void update_Baby_editHN(string host, int port, string database, string hn_from, int ord_from, string hn_change, int ord_change)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<Baby>("dt_baby");
                var filter = Builders<Baby>.Filter.Eq(c => c.HN, hn_from) & Builders<Baby>.Filter.Eq(c => c.Order, ord_from);
                var update = Builders<Baby>.Update.Set("patient_hn", hn_change).Set("order", ord_change);
                var result = collect.UpdateOneAsync(filter, update);
            }
        }

        // Use
        public void update_TagZone_editHN(string host, int port, string database, string hn_from, int ord_from, string hn_change, int ord_change)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<tag_zone>("dt_tag_zone");
                var filter = Builders<tag_zone>.Filter.Eq(c => c.patient_hn, hn_from) & Builders<tag_zone>.Filter.Eq(c => c.order, ord_from);
                var update = Builders<tag_zone>.Update.Set("patient_hn", hn_change).Set("order", ord_change);
                var result = collect.UpdateOneAsync(filter, update);
            }
        }

        // Use
        public void update_Transition_editHN(string host, int port, string database, string hn_from, int ord_from, string hn_change, int ord_change)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<transition>("dt_transition");
                var filter = Builders<transition>.Filter.Eq(c => c.patient_hn, hn_from) & Builders<transition>.Filter.Eq(c => c.order, ord_from);
                var update = Builders<transition>.Update.Set("patient_hn", hn_change).Set("order", ord_change);
                var result = collect.UpdateManyAsync(filter, update);
            }
        }

        // Use
        public void update_TagZoneLog_editHN(string host, int port, string database, string hn_from, int ord_from, string hn_change, int ord_change)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<tag_zone>("dt_tag_zone_logs");
                var filter = Builders<tag_zone>.Filter.Eq(c => c.patient_hn, hn_from) & Builders<tag_zone>.Filter.Eq(c => c.order, ord_from);
                var update = Builders<tag_zone>.Update.Set("patient_hn", hn_change).Set("order", ord_change);
                var result = collect.UpdateManyAsync(filter, update);
            }
        }

        // Use
        public void update_BabyStateLog_editHN(string host, int port, string database, string hn_from, int ord_from, string hn_change, int ord_change)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<Baby_state_log>("dt_baby_state_log");
                var filter = Builders<Baby_state_log>.Filter.Eq(c => c.patient_hn, hn_from) & Builders<Baby_state_log>.Filter.Eq(c => c.order, ord_from);
                var update = Builders<Baby_state_log>.Update.Set("patient_hn", hn_change).Set("order", ord_change);
                var result = collect.UpdateManyAsync(filter, update);
            }
        }

        // Use
        public void update_Bed_editHN(string host, int port, string database, string hn_from, int ord_from, string hn_change, int ord_change)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<Bed>("dt_bed");
                var filter = Builders<Bed>.Filter.Eq(c => c.patient_hn, hn_from) & Builders<Bed>.Filter.Eq(c => c.order, ord_from);
                var update = Builders<Bed>.Update.Set("patient_hn", hn_change).Set("order", ord_change);
                var result = collect.UpdateManyAsync(filter, update);
            }
        }

        // Use
        public void update_UIAlert_editHN(string host, int port, string database, string hn_from, int ord_from, string hn_change, int ord_change)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<alert_events>("ui_alerts");
                var filter = Builders<alert_events>.Filter.Eq(c => c.hn, hn_from) & Builders<alert_events>.Filter.Eq(c => c.order, ord_from);
                var update = Builders<alert_events>.Update.Set("hn", hn_change).Set("order", ord_change);
                var result = collect.UpdateManyAsync(filter, update);
            }
        }

        // Use
        public void update_UILog_editHN(string host, int port, string database, string hn_from, int ord_from, string hn_change, int ord_change)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<zone_log>("ui_log_patient");
                var filter = Builders<zone_log>.Filter.Eq(c => c.patient_hn, hn_from) & Builders<zone_log>.Filter.Eq(c => c.order, ord_from);
                var update = Builders<zone_log>.Update.Set("patient_hn", hn_change).Set("order", ord_change);
                var result = collect.UpdateManyAsync(filter, update);
            }
        }

        // Use
        public void update_UIAlertLog_editHN(string host, int port, string database, string hn_from, int ord_from, string hn_change, int ord_change)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<alert_log>("dt_alert_log");
                var filter = Builders<alert_log>.Filter.Eq(c => c.patient_hn, hn_from) & Builders<alert_log>.Filter.Eq(c => c.order, ord_from);
                var update = Builders<alert_log>.Update.Set("patient_hn", hn_change).Set("order", ord_change);
                var result = collect.UpdateManyAsync(filter, update);
            }
        }

        /// <summary>
        /// Query UI_Display objectsv List
        /// </summary>
        /// <returns></returns>

        // Use
        public List<transition> get_Transition(string host, int port, string database, string tag_serial)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<transition>("dt_transition");
                var result = collect.Find(x => (x.tag_serial == tag_serial)).ToList();
                return result;
            }
            else return null;
        }

        // Use
        public List<transition> get_Transition(string host, int port, string database, string hn, int ord)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<transition>("dt_transition");
                var result = collect.Find(x => (x.patient_hn == hn) && (x.order == ord)).ToList();
                return result;
            }
            else return null;
        }

        // Use
        public List<beacon_location> get_BeaconLocation(string host, int port, string database, string type, int fl)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<beacon_location>("it_beacon_location");
                var result = collect.Find(x => (x.beacon_type == type) && (x.floor_fl == fl)).ToList();
                return result;
            }
            else return null;
        }

        // Use
        public List<beacon_location> get_BeaconLocation_AllFl(string host, int port, string database, string type)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<beacon_location>("it_beacon_location");
                var result = collect.Find(x => (x.beacon_type == type)).ToList();
                return result;
            }
            else return null;
        }

        // Use
        public List<beacon_location> get_BeaconLocation_All(string host, int port, string database)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<beacon_location>("it_beacon_location");
                var result = collect.Find(new BsonDocument()).ToList();
                return result;
            }
            else return null;
        }

        public List<beacon_location> get_BaconLocation_typeNR()
        {
            if (connectDB())
            {
                var collect = db.GetCollection<beacon_location>("it_beacon_location");
                var result = collect.Find(x => (x.beacon_type == "N") || (x.beacon_type == "R")).ToList();
                return result;
            }
            else return null;
        }

        // Use
        public List<Baby> get_Baby(string host, int port, string database, string hn, int order)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<Baby>("dt_baby");
                var result = collect.Find(x => (x.HN == hn) && (x.Order == order)).ToList();
                return result;
            }
            else return null;
        }

        // Use
        public List<Baby> get_Baby_w_momHN(string host, int port, string database, string hn)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<Baby>("dt_baby");
                var result = collect.Find(x => x.Mother_HN == hn).ToList();
                return result;
            }
            else return null;
        }

        // Use
        public List<Data> get_Data(string host, int port, string database)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<Data>("dt_data");
                var result = collect.Find(new BsonDocument()).ToList();
                return result;
            }
            else return null;
        }

        // Use
        public List<discharge> get_Discharge(string host, int port, string database, string hn, int ord)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<discharge>("dt_discharge");
                var result = collect.Find(x => (x.patient_hn == hn) && (x.order == ord)).ToList();
                return result;
            }
            else return null;
        }

        public List<Beacon_Zone> get_BeaconZone(string code)
        {
            if (connectDB())
            {
                var collect = db.GetCollection<Beacon_Zone>("it_beacon_zones");
                var result = collect.Find(x => x.ZoneCode == code).ToList();
                return result;
            }
            else return null;
        }

        public List<Patient> get_Patient_all()
        {
            if (connectDB())
            {
                var collect = db.GetCollection<Patient>("dt_patient");
                var result = collect.Find(new BsonDocument()).ToList();
                return result;
            }
            else return null;
        }

        // query List<patient> by no in Patient collection
        public Patient get_Patient(string hn)
        {
            if (connectDB())
            {
                var collect = db.GetCollection<Patient>("dt_patient");
                var result = collect.Find(x => x.hn == hn).ToList();
                if (result.Count > 0)
                {
                    return result[0];
                }
                else
                {
                    return null;
                }
            }
            else return null;
        }

        // Use
        public List<Patient> get_Patient_by_Mom(string host, int port, string database, string hn)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<Patient>("dt_patient");
                var result = collect.Find(x => (x.hn == hn) && (x.role == (int)tag_role.mother)).ToList();
                return result;
            }
            else return null;
        }

        // Use
        public List<lis_state> get_BabyStateLog(string host, int port, string database, string hn, int ord)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<lis_state>("dt_baby_state_log");
                var result = collect.Find(x => (x.patient_hn == hn) && (x.order == ord)).ToList();
                return result;
            }
            else return null;
        }

        // USe
        public List<Patient> get_Patient_Child(string host, int port, string database, string hn, int ord)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<Patient>("dt_patient");
                var result = collect.Find(x => (x.hn == hn) && (x.order == ord)).ToList();
                return result;
            }
            else return null;
        }

        public List<discharge> get_Discharge()
        {
            if (connectDB())
            {
                var collect = db.GetCollection<discharge>("dt_discharge");
                var result = collect.Find(new BsonDocument()).ToList();
                return result;
            }
            else return null;
        }

        // Use
        public List<command_beacon> get_CommandBC(string host, int port, string database, string bc_type, string bc)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<command_beacon>("it_command_beacons");
                var result = collect.Find(x => (x.beacon_type == bc_type) && (x.beacon_serial == bc)).ToList();
                return result;
            }
            else return null;
        }

        // Use
        public List<PatientDevice_Group> get_PatientDeviceGroup_hn(string host, int port, string database, string hn, int ord)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<PatientDevice_Group>("dt_patient_device_groups");
                var result = collect.Find(x => (x.PatienHN == hn) && (x.Order == ord)).ToList();
                return result;
            }
            else return null;
        }

        // Use
        public List<alert_log> get_AlertLog(string host, int port, string database, string sn)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<alert_log>("dt_alert_log");
                var result = collect.Find(x => x.serial == sn).ToList();
                return result;
            }
            else return null;
        }

        // Use
        public List<alert_events> get_UIAlert(string host, int port, string database, string sn)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<alert_events>("ui_alerts");
                var result = collect.Find(x => x.serial == sn).ToList();
                return result;
            }
            else return null;
        }

        // Use
        public List<alert_events> get_UIAlert(string host, int port, string database, string hn, int ord)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<alert_events>("ui_alerts");
                var result = collect.Find(x => (x.hn == hn) && (x.order == ord)).ToList();
                return result;
            }
            else return null;
        }

        // query List<patient_device_group> by role = 1 (child) in PatientDevideGroup collection
        public List<PatientDevice_Group> get_PatientDeviceGroup_all_child()
        {
            if (connectDB())
            {
                var collect = db.GetCollection<PatientDevice_Group>("patient_device_group");
                var result = collect.Find(x => x.Role == tag_role.child).ToList();
                return result;
            }
            else return null;
        }

        // query List<tag> by serial in Tag collection
        public List<Tag> get_Tag(string se)
        {
            if (connectDB())
            {
                var collect = db.GetCollection<Tag>("tag");
                var result = collect.Find(x => x.Serial == se).ToList();
                return result;
            }
            else return null;
        }

        // query all List<tag_zone> sort by tag_id in TagZone collection
        public List<tag_zone> get_TagZone_all()
        {
            if (connectDB())
            {
                var collect = db.GetCollection<tag_zone>("tag_zone");
                List<tag_zone> result = new List<tag_zone>();
                result = collect.Find(new BsonDocument()).SortByDescending(x => x.tag_serial).ToList();
                return result;
            }
            else return null;
        }

        // Use
        public List<lis_ticket> get_Ticket(string host, int port, string database, string hn)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<lis_ticket>("dt_tickets");
                var result = collect.Find(x => x.mom_hn == hn).ToList();
                return result;
            }
            else return null;
        }

        // Use
        public List<lis_vertwo_log> get_Ver2Logs(string host, int port, string database, string sn)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<lis_vertwo_log>("dt_vertwo_log");
                var result = collect.Find(x => x.sn == sn).ToList();
                return result;
            }
            else return null;
        }

        // Use
        public List<zone_log> get_UILogPatient(string host, int port, string database, string hn, int ord)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<zone_log>("ui_log_patient");
                var result = collect.Find(x => (x.patient_hn == hn) && (x.order == ord)).ToList();
                return result;
            }
            else return null;
        }

        // Use
        public List<tag_zone> get_TagZoneLog(string host, int port, string database, string sn)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<tag_zone>("dt_tag_zone_logs");
                var result = collect.Find(x => x.tag_serial == sn).ToList();
                return result;
            }
            else return null;
        }

        // query all List<tag_zone> sort by tag_id in TagZone collection
        public List<tag_zone> get_TagZoneLog_all()
        {
            if (connectDB())
            {
                var collect = db.GetCollection<tag_zone>("tag_zone_log");
                List<tag_zone> result = new List<tag_zone>();
                result = collect.Find(new BsonDocument()).SortByDescending(x => x.tag_serial).ToList();
                return result;
            }
            else return null;
        }

        // query List<zone> by floor_is in Zone collection
        public List<Zone> get_Zone_fl(int fl)
        {
            if (connectDB())
            {
                var collect = db.GetCollection<Zone>("zone");
                var result = collect.Find(x => x.Floor == fl).ToList();
                return result;
            }
            else return null;
        }

        // Use
        public List<Zone> get_Zone(string host, int port, string database, string code)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<Zone>("it_zones");
                var result = collect.Find(x => x.Code == code).ToList();
                return result;
            }
            else return null;
        }
        #endregion

        #region Insert Method
        
        // Use
        public void insert_many(string host, int port, string database, string collection_name, BsonDocument[] obj)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<BsonDocument>(collection_name);
                var result = collect.InsertManyAsync(obj);
            }
        }

        // Use
        public void insert_one(string host, int port, string database, string collection_name, BsonDocument obj)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<BsonDocument>(collection_name);
                var result = collect.InsertOneAsync(obj);
            }
        }

        #endregion

        #region Delete Method

        // Use
        public void delete_Tag(string host, int port, string database, string se)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<Tag>("dt_tags");
                var result = collect.DeleteManyAsync(x => x.Serial == se);
            }
        }

        // Use
        public void delete_PDG(string host, int port, string database, string se)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<PatientDevice_Group>("dt_patient_device_groups");
                var result = collect.DeleteManyAsync(x => x.TagSerial == se);
            }
        }

        // Use
        public void delete_PDG(string host, int port, string database, string hn, int ord)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<PatientDevice_Group>("dt_patient_device_groups");
                var result = collect.DeleteManyAsync(x => (x.PatienHN == hn) && (x.Order == ord));
            }
        }

        // Use
        public void delete_Baby(string host, int port, string database, string hn, int ord)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<Baby>("dt_baby");
                var result = collect.DeleteManyAsync(x => (x.HN == hn) && (x.Order == ord));
            }
        }

        // Use
        public void delete_Patient(string host, int port, string database, string hn, int ord)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<Patient>("dt_patient");
                var result = collect.DeleteManyAsync(x => (x.hn == hn) && (x.order == ord));
            }
        }

        // Use
        public void delete_AlertLog(string host, int port, string database, string se)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<alert_log>("dt_alert_log");
                var result = collect.DeleteManyAsync(x => x.serial == se);
            }
        }

        // Use
        public void delete_BabyStateLog(string host, int port, string database, string hn, int ord)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<lis_state>("dt_baby_state_log");
                var result = collect.DeleteManyAsync(x => (x.patient_hn == hn) && (x.order == ord));
            }
        }

        // Use
        public void delete_Bed(string host, int port, string database, string hn, int ord)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<Bed>("dt_bed");
                var result = collect.DeleteManyAsync(x => (x.patient_hn == hn) && (x.order == ord));
            }
        }

        // Use
        public void delete_Discharge(string host, int port, string database, string se)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<discharge>("dt_discharge");
                var result = collect.DeleteManyAsync(x => x.tag_serial == se);
            }
        }

        // Use
        public void delete_TagZone(string host, int port, string database, string se)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<tag_zone>("dt_tag_zone");
                var result = collect.DeleteManyAsync(x => x.tag_serial == se);
            }
        }

        // Use
        public void delete_TagZoneLog(string host, int port, string database, string se)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<tag_zone>("dt_tag_zone_logs");
                var result = collect.DeleteManyAsync(x => x.tag_serial == se);
            }
        }

        // Use
        public void delete_TagZoneLogTemp(string host, int port, string database, string se)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<tag_zone_log>("dt_tag_zone_logs_temp");
                var result = collect.DeleteManyAsync(x => x.tag_serial == se);
            }
        }

        // Use
        public void delete_Transition(string host, int port, string database, string se)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<transition>("dt_transition");
                var result = collect.DeleteManyAsync(x => x.tag_serial == se);
            }
        }

        // Use
        public void delete_Ticket(string host, int port, string database, string hn)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<lis_ticket>("dt_tickets");
                var result = collect.DeleteManyAsync(x => x.mom_hn == hn);
            }
        }

        // Use
        public void delete_Vertwolog(string host, int port, string database, string sn)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<lis_vertwo_log>("dt_vertwo_log");
                var result = collect.DeleteManyAsync(x => x.sn == sn);
            }
        }

        // Use
        public void delete_UIAlert(string host, int port, string database, string se)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<alert_events>("ui_alerts");
                var result = collect.DeleteManyAsync(x => x.serial == se);
            }
        }

        // Use
        public void delete_UILogPatient(string host, int port, string database, string hn, int ord)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<zone_log>("ui_log_patient");
                var result = collect.DeleteManyAsync(x => (x.patient_hn == hn) && (x.order == ord));
            }
        }

        // Use
        public void delete_Unnecessary_data(string host, int port, string database, string coll)
        {
            if (connectDB(host, port, database))
            {
                var collect = db.GetCollection<BsonDocument>(coll);
                var result = collect.DeleteManyAsync(new BsonDocument());
            }
        }


        // delete all data in collection
        public void delete_all()
        {
            if (connectDB())
            {
                var collect = db.GetCollection<BsonDocument>("baby");
                var filter = new BsonDocument();                                        // all filter
                var result = collect.DeleteManyAsync(filter);

                collect = db.GetCollection<BsonDocument>("beacon");
                result = collect.DeleteManyAsync(filter);

                collect = db.GetCollection<BsonDocument>("beacon_zone");
                result = collect.DeleteManyAsync(filter);

                collect = db.GetCollection<BsonDocument>("floor");
                result = collect.DeleteManyAsync(filter);

                collect = db.GetCollection<BsonDocument>("patient");
                result = collect.DeleteManyAsync(filter);

                collect = db.GetCollection<BsonDocument>("patient_device_group");
                result = collect.DeleteManyAsync(filter);

                collect = db.GetCollection<BsonDocument>("person_type");
                result = collect.DeleteManyAsync(filter);

                collect = db.GetCollection<BsonDocument>("tag");
                result = collect.DeleteManyAsync(filter);

                collect = db.GetCollection<BsonDocument>("users");
                result = collect.DeleteManyAsync(filter);

                collect = db.GetCollection<BsonDocument>("zone");
                result = collect.DeleteManyAsync(filter);

                collect = db.GetCollection<BsonDocument>("zone_type");
                result = collect.DeleteManyAsync(filter);
            }
        }

        // delete tag_id that no problem in Events
        public void delete_Events(string se)
        {
            if (connectDB())
            {
                var collect = db.GetCollection<alert_events>("ui_alerts");
                var result = collect.DeleteManyAsync(x => x.serial == se);
            }
        }

        // delete event in Datas
        public void delete_topic_event_Datas()
        {
            if (connectDB())
            {

                var collect = db.GetCollection<Data>("dt_data");
                var result = collect.DeleteManyAsync(x => x.Topic == topic.data);
            }
        }

        // delete data in Datas
        public void delete_topic_data_Datas()
        {
            if (connectDB())
            {

                var collect = db.GetCollection<Data>("dt_data");
                var result = collect.DeleteManyAsync(x => x.Topic == topic.data);
            }
        }

        // delete data in TagZone
        public void delete_TagZone()
        {
            if (connectDB())
            {
                var collect = db.GetCollection<Data>("tag_zone");
                var result = collect.DeleteManyAsync(new BsonDocument());
            }
        }
        #endregion
    } // ./class bbt_DB

    internal class DBStats
    {
        internal string db { get; set; }
        internal int collections { get; set; }
        internal int objects { get; set; }
        internal double avgObjSize { get; set; }
        internal double dataSize { get; set; }
        internal double storageSize { get; set; }
        internal int numExtents { get; set; }
        internal int indexes { get; set; }
        internal double indexSize { get; set; }
        internal double ok { get; set; }
    }
}
