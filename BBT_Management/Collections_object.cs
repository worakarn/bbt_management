﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Globalization;
using System.Net;
using System.Net.Sockets;
using System.Drawing;

namespace BBT_Management
{
    public class beacon
    {
        public ObjectId _id { get; set; }
        public string serial { get; set; }
        public DateTime lastsyn { get; set; }
        public int isActive { get; set; }
        public string ip { get; set; }
        public int setting { get; set; }
        public DateTime lastCatch { get; set; }
        public int command { get; set; }
        public DateTime lastReset { get; set; }
        public int reset { get; set; }
        public int resp { get; set; }
        public DateTime lastResp { get; set; }
    }

    // Use
    public class Beacon_Zone
    {
        [BsonId]
        public ObjectId _id { get; set; }

        [BsonElement("beacon_serial")]
        public string Serial { get; set; }

        [BsonElement("zone_zone_code")]
        public string ZoneCode { get; set; }

        public override string ToString()
        {
            return ZoneCode + " - " + Serial;
        }
    }

    // Use
    public class Zone
    {
        [BsonId]
        public ObjectId _id { get; set; }

        [BsonElement("zone_name")]
        public string Name { get; set; }

        [BsonElement("zone_code")]
        public string Code { get; set; }

        [BsonElement("floor_fl")]
        public int Floor { get; set; }

        [BsonElement("zone_type_priority")]
        public int Priority { get; set; }

        [BsonElement("zone_boundary_priority")]
        public int Boundary { get; set; }
    }
    
    // Use
    public class Baby
    {
        [BsonId]
        public ObjectId _id { get; set; }

        [BsonElement("patient_hn")]
        public string HN { get; set; }

        [BsonElement("mom_patient_hn")]
        public string Mother_HN { get; set; }

        [BsonElement("near_by_mom")]
        public int Near_mom { get; set; }

        [BsonElement("status")]
        public baby_status Status { get; set; }

        [BsonElement("order")]
        public int Order { get; set; }

        [BsonElement("upd_hn")]
        public int Upd_HN { get; set; }

        [BsonElement("bond_date")]
        public DateTime Bond_Date { get; set; }

        [BsonElement("discharge_date")]
        public DateTime Dis_Date { get; set; }

        [BsonElement("state")]
        public int State { get; set; }

        [BsonElement("low_batt_trigger")]
        public int Low_batt_trig { get; set; }

        [BsonElement("tag_loose_trigger")]
        public int tag_loose_trigger { get; set; }

        [BsonElement("strap_cut_trigger")]
        public int strap_cut_trigger { get; set; }

        public override string ToString()
        {
            return String.Format("{0} - {1}", Status, HN);
        }

    }

    // Use
    public class Baby_state_log
    {
        public ObjectId _id { get; set; }
        public string patient_hn { get; set; }
        public int order { get; set; }
        public int state { get; set; }
        public DateTime timestamp { get; set; }
    }

    // Use
    public class Tag
    {
        [BsonId]
        public ObjectId _id { get; set; }

        [BsonElement("serial")]
        public string Serial { get; set; }
    }

    // Use
    public class Patient
    {
        public ObjectId _id { get; set; }
        public string hn { get; set; }
        public string prefix { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string gender { get; set; }
        public string birth_str { get; set; }
        public DateTime birth_dt { get; set; }
        public string an { get; set; }
        public string bed { get; set; }
        public DateTime discharge_dt { get; set; }
        public string an_pair { get; set; }
        public string hn_pair { get; set; }
        public int role { get; set; }
        public int order { get; set; }
    }

    // Use
    public class Bed
    {
        public ObjectId _id { get; set; }
        public DateTime timestamp { get; set; }
        public string patient_hn { get; set; }
        public string bed { get; set; }
        public int role { get; set; }
        public int order { get; set; }
        public int floor { get; set; }
    }

    // Use
    public class PatientDevice_Group
    {
        [BsonId]
        public ObjectId _id { get; set; }

        [BsonElement("tag_serial")]
        public string TagSerial { get; set; }

        [BsonElement("patient_hn")]
        public string PatienHN { get; set; }

        [BsonElement("role")]
        public tag_role Role { get; set; }

        [BsonElement("order")]
        public int Order { get; set; }

        [BsonElement("upd_hn")]
        public int Update_HN { get; set; }

        [BsonElement("near")]
        public int Near { get; set; }

        [BsonElement("near_serial")]
        public string Near_Serial { get; set; }

        [BsonElement("unauthorized_trigger")]
        public int Unauthorized_Trigger { get; set; }

        [BsonElement("prohibit_trigger")]
        public int Prohibit_Trigger { get; set; }

        [BsonElement("near_portal_trigger")]
        public int Near_Portal_Trigger { get; set; }

        public override string ToString()
        {
            return String.Format("({0}-{1}){2} - {3}", Role, Order, TagSerial, PatienHN);
        }
    }

    // Use
    public class Data
    {
        [BsonId]
        public object _id { get; set; }

        [BsonElement("ver")]
        public string Version { get; set; }

        [BsonElement("ts")]
        public string _timestamp { get; set; }

        [BsonElement("src")]
        public DataSource Source { get; set; }

        [BsonElement("mthd")]
        public method Method { get; set; }

        [BsonElement("tpc")]
        public topic Topic { get; set; }

        [BsonElement("cont")]
        public List<ContentObject> Contents { get; set; }

        [BsonElement("timestamp")]
        public DateTime TimeStamp { get; set; }


    }

    // Use
    public class DataSource
    {
        [BsonElement("scc")]
        public scc Class { get; set; }

        [BsonElement("ip")]
        public string IP { get; set; }

        [BsonElement("sn")]
        public string Serial { get; set; }

        public override string ToString()
        {
            return String.Format("{0}-{1}-{2}", Class.ToString().ToUpper(), Serial.ToUpper(), IP);
        }

        public string toJSON()
        {
            StringBuilder data_json = new StringBuilder();
            data_json.Append("\"src\":{");
            data_json.Append(String.Format("\"{0}\":{1},", "scc", (int)Class));
            data_json.Append(String.Format("\"{0}\":\"{1}\",", "ip", IP));
            data_json.Append(String.Format("\"{0}\":\"{1}\"", "sn", Serial));
            data_json.Append("}");

            return data_json.ToString();
        }

    }

    // Use
    public class ContentObject
    {
        public scc scc { get; set; }
        public string sn { get; set; }
        public int rssi { get; set; }
        public double bp { get; set; }
        public event_type evnt { get; set; }

        public override string ToString()
        {
            return String.Format("{0} - {1}", sn, (sbyte)rssi);
        }
    } // ./class lis_data

    // Use
    public class RSSI_checking
    {
        public string bc_serial { get; set; }
        public string tag_serial { get; set; }
        public int rssi { get; set; }
        public string code { get; set; }
        public int evnt { get; set; }
    }

    // Use
    public class tag_zone
    {
        public ObjectId _id { get; set; }
        public string tag_serial { get; set; }
        public string patient_hn { get; set; }
        public int order { get; set; }
        public int near { get; set; }
        public string near_serial { get; set; }
        public int near_portal_trigger { get; set; }
        public string beacon_serial { get; set; }
        public string zone_zone_code { get; set; }
        public int rssi { get; set; }
        public double battery { get; set; }
        public List<string> lis_bc_1st { get; set; }
        public string lis_bc_ty_1st { get; set; }
        public List<string> lis_code_1st { get; set; }
        public List<int> lis_rssi_1st { get; set; }
        public List<string> lis_bc_2nd { get; set; }
        public string lis_bc_ty_2nd { get; set; }
        public List<string> lis_code_2nd { get; set; }
        public List<int> lis_rssi_2nd { get; set; }
        public int trigger { get; set; }
        public DateTime timestamp { get; set; }
    }

    // Use
    public class tag_zone_log
    {
        public ObjectId _id { get; set; }
        public string tag_serial { get; set; }
        public string beacon_serial { get; set; }
        public string zone_zone_code { get; set; }
        public string zone_name { get; set; }
        public int trigger_change { get; set; }
        public DateTime timestamp { get; set; }
    }

    // Use
    public class zone_log
    {
        public ObjectId _id { get; set; }
        public string patient_hn { get; set; }
        public int order { get; set; }
        public string location_log { get; set; }
        public string date_log { get; set; }
        public string time_log { get; set; }
    }

    // Use
    public class transition
    {
        public ObjectId _id { get; set; }
        public string role { get; set; }
        public int IsActive { get; set; }
        public DateTime current_time { get; set; }
        public string dep { get; set; }
        public int duration_hour { get; set; }
        public int duration_min { get; set; }
        public int duration_sec { get; set; }
        public string from_loc { get; set; }
        public string from_loc_name { get; set; }
        public string nurse_name { get; set; }
        public int order { get; set; }
        public string patient_hn { get; set; }
        public string tag_serial { get; set; }
        public string to_loc { get; set; }
        public string to_loc_name { get; set; }
        public DateTime stop_time { get; set; }
        public string from_loc_type { get; set; }
        public string to_loc_type { get; set; }
    }
   
    // Use
    public class alert_events
    {

        public ObjectId _id { get; set; }
        public string incident { get; set; }
        public string location { get; set; }
        public string time { get; set; }
        public string serial { get; set; }
        public int event_type { get; set; }
        public string user { get; set; }
        public int fl { get; set; }
        public string zone_code { get; set; }
        public DateTime timestamp { get; set; }
        public string name { get; set; }
        public string hn { get; set; }
        public int order { get; set; }
        public string order_str { get; set; }
        public int resolved { get; set; }
        public string remark { get; set; }
        public int device_type { get; set; }
    }

    // Use
    public class beacon_location
    {
        public ObjectId _id { get; set; }
        public string beacon_serial { get; set; }
        public string beacon_type { get; set; }
        public string code { get; set; }
        public int floor_fl { get; set; }
        public int status { get; set; }
        public int bias { get; set; }
    }

    // Use
    public class discharge
    {
        public ObjectId _id { get; set; }
        public string patient_hn { get; set; }
        public int order { get; set; }
        public string tag_serial { get; set; }
        public DateTime timestamp { get; set; }
        public string nurse_name { get; set; }
        public string dep { get; set; }
        public lis_mom mother { get; set; }
        public List<Patient> patient { get; set; }
        public List<lis_state> state { get; set; }
        public List<Bed> bed { get; set; }
        public List<transition> transition { get; set; }
        public List<zone_log> zone_log { get; set; }
        public List<tag_zone> tag_zone_log { get; set; }
        public List<alert_events> alert { get; set; }
        public List<alert_log> alert_resolve { get; set; }
        public List<lis_ticket> ticket { get; set; }
        public List<lis_vertwo_log> vertwo_log { get; set; }
    }

    public class lis_ticket
    {
        public string _id { get; set; }
        public string mom_hn { get; set; }
        public obj_ticket ticket { get; set; }
        public List<lis_tag> list_tag { get; set; }
        public int activated { get; set; }
        public DateTime ts { get; set; }
    }

    public class obj_ticket
    {
        public List<obj_boardcast> obj { get; set; }
        public string key { get; set; }
        public string led_frq { get; set; }
        public string color { get; set; }
        public string ts { get; set; }
        public string token { get; set; }
    }

    public class obj_boardcast
    {
        public string b_str { get; set; }
        public int role { get; set; }
        public string auth_frq { get; set; }
    }

    public class lis_tag
    {
        public string sn { get; set; }
        public int scc { get; set; }
    }

    public class lis_vertwo_log
    {
        public object _id { get; set; }
        public DateTime ts { get; set; }
        public string sn { get; set; }
        public int batt { get; set; }
        public int distance { get; set; }
    }

    // Use
    public class lis_mom
    {
        public List<Patient> patient { get; set; }
        public List<Bed> bed { get; set; }
    }

    // Use
    public class lis_state
    {
        public object _id { get; set; }
        public string patient_hn { get; set; }
        public int order { get; set; }
        public int state { get; set; }
        public DateTime timestamp { get; set; }
    }

    // Use
    public class alert_log
    {
        public ObjectId _id { get; set; }
        public int floor_fl { get; set; }
        public string zone_zone_code { get; set; }
        public string serial { get; set; }
        public string patient_hn { get; set; }
        public int order { get; set; }
        public DateTime timestamp_start { get; set; }
        public DateTime timestamp_end { get; set; }
        public string scid { get; set; }
        public int event_type { get; set; }
        public string remark { get; set; }
    }

    // Use
    public class command_beacon
    {
        public ObjectId _id { get; set; }
        public string beacon_serial { get; set; }
        public string beacon_type { get; set; }
        public int cnt { get; set; }
        public proto_obj proto { get; set; }
    }

    // Use
    public class proto_obj
    {
        public string ver { get; set; }
        public string ts { get; set; }
        public int mthd { get; set; }
        public int tpc { get; set; }
        public int cmd { get; set; }
        public cont_obj cont { get; set; }
    }

    // Use
    public class cont_obj
    {
        public int dur { get; set; } 
        public string lmode { get; set; }
        public string lcolor { get; set; }
        public int lperiod { get; set; }
        public string smode { get; set; }
        public int sontime { get; set; }
        public int sofftime { get; set; }
    }
}
