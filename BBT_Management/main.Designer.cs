﻿namespace BBT_Management
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cb_hos = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_port = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bt_cancel_dis = new System.Windows.Forms.Button();
            this.bt_dis = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.bt_cancel_trans = new System.Windows.Forms.Button();
            this.bt_tm_trans = new System.Windows.Forms.Button();
            this.bt_edit_trans = new System.Windows.Forms.Button();
            this.bt_trans = new System.Windows.Forms.Button();
            this.bt_edit_tag_lost = new System.Windows.Forms.Button();
            this.bt_edit_tag_login = new System.Windows.Forms.Button();
            this.bt_edit_room_out = new System.Windows.Forms.Button();
            this.bt_bc_mana = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.bt_edit_tag_sn = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.bt_edit_room = new System.Windows.Forms.Button();
            this.bt_edit_hn_child = new System.Windows.Forms.Button();
            this.bt_clear_col_db = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_hn = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_ord = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.bt_add_gate = new System.Windows.Forms.Button();
            this.bt_add_bc = new System.Windows.Forms.Button();
            this.bt_change_gate = new System.Windows.Forms.Button();
            this.bt_change_bc = new System.Windows.Forms.Button();
            this.bt_check_rssi = new System.Windows.Forms.Button();
            this.bt_check_led_and_sound = new System.Windows.Forms.Button();
            this.bt_back_home = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.bt_resolve_alert = new System.Windows.Forms.Button();
            this.bt_mute_alert = new System.Windows.Forms.Button();
            this.pn_bc_mana = new System.Windows.Forms.Panel();
            this.pn_trans = new System.Windows.Forms.Panel();
            this.pn_check_R_G = new System.Windows.Forms.Panel();
            this.pn_check_rssi = new System.Windows.Forms.Panel();
            this.pn_edit = new System.Windows.Forms.Panel();
            this.pn_edit_room = new System.Windows.Forms.Panel();
            this.bt_edit_room_ok = new System.Windows.Forms.Button();
            this.tb_edit_room = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.bt_edit_room_back = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.tb_ord_edit_hn_change = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tb_ord_edit_hn_from = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.rb_edit_hn_child = new System.Windows.Forms.RadioButton();
            this.rb_edit_sn_tag = new System.Windows.Forms.RadioButton();
            this.bt_edit_back = new System.Windows.Forms.Button();
            this.bt_edit_ok = new System.Windows.Forms.Button();
            this.tb_edit_change = new System.Windows.Forms.TextBox();
            this.tb_edit_from = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lb_timer = new System.Windows.Forms.Label();
            this.rb_sort_code = new System.Windows.Forms.RadioButton();
            this.lb_tic_toc = new System.Windows.Forms.Label();
            this.rb_sort_rssi = new System.Windows.Forms.RadioButton();
            this.rb_sort_tag = new System.Windows.Forms.RadioButton();
            this.rb_sort_bc = new System.Windows.Forms.RadioButton();
            this.label10 = new System.Windows.Forms.Label();
            this.dg_rssi = new System.Windows.Forms.DataGridView();
            this.col_bc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_tag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_rssi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Event = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gb_rssi_select = new System.Windows.Forms.GroupBox();
            this.cb_rssi_select_tag = new System.Windows.Forms.ComboBox();
            this.cb_rssi_select_bc = new System.Windows.Forms.ComboBox();
            this.rb_rssi_select_tag = new System.Windows.Forms.RadioButton();
            this.rb_rssi_select_bc = new System.Windows.Forms.RadioButton();
            this.bt_back_rssi = new System.Windows.Forms.Button();
            this.gb_bc_type = new System.Windows.Forms.GroupBox();
            this.rb_t = new System.Windows.Forms.RadioButton();
            this.rb_l = new System.Windows.Forms.RadioButton();
            this.rb_b = new System.Windows.Forms.RadioButton();
            this.rb_h = new System.Windows.Forms.RadioButton();
            this.rb_p = new System.Windows.Forms.RadioButton();
            this.rb_n = new System.Windows.Forms.RadioButton();
            this.rb_x = new System.Windows.Forms.RadioButton();
            this.rb_g = new System.Windows.Forms.RadioButton();
            this.rb_r = new System.Windows.Forms.RadioButton();
            this.gb_cmd = new System.Windows.Forms.GroupBox();
            this.rb_reset = new System.Windows.Forms.RadioButton();
            this.rb_on_work = new System.Windows.Forms.RadioButton();
            this.rb_on_led = new System.Windows.Forms.RadioButton();
            this.rb_off_led = new System.Windows.Forms.RadioButton();
            this.rb_off_sound = new System.Windows.Forms.RadioButton();
            this.rb_on_sound = new System.Windows.Forms.RadioButton();
            this.lb_show_led = new System.Windows.Forms.Label();
            this.cb_list_bc = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.bt_back_select_fl = new System.Windows.Forms.Button();
            this.bt_ok_or_stop = new System.Windows.Forms.Button();
            this.cb_select_fl = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.bt_OK_trans = new System.Windows.Forms.Button();
            this.cb_trans_to = new System.Windows.Forms.ComboBox();
            this.cb_trans_from = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.bt_back_home_trans = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.tb_edit_room_fl = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.pn_bc_mana.SuspendLayout();
            this.pn_trans.SuspendLayout();
            this.pn_check_R_G.SuspendLayout();
            this.pn_check_rssi.SuspendLayout();
            this.pn_edit.SuspendLayout();
            this.pn_edit_room.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_rssi)).BeginInit();
            this.gb_rssi_select.SuspendLayout();
            this.gb_bc_type.SuspendLayout();
            this.gb_cmd.SuspendLayout();
            this.SuspendLayout();
            // 
            // cb_hos
            // 
            this.cb_hos.FormattingEnabled = true;
            this.cb_hos.Location = new System.Drawing.Point(81, 13);
            this.cb_hos.Name = "cb_hos";
            this.cb_hos.Size = new System.Drawing.Size(90, 21);
            this.cb_hos.TabIndex = 2;
            this.cb_hos.Text = "-- Selected --";
            this.cb_hos.SelectedIndexChanged += new System.EventHandler(this.cb_hos_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "โรงพยาบาล";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(185, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "PORT  :";
            // 
            // tb_port
            // 
            this.tb_port.Location = new System.Drawing.Point(237, 12);
            this.tb_port.Name = "tb_port";
            this.tb_port.Size = new System.Drawing.Size(57, 20);
            this.tb_port.TabIndex = 5;
            this.tb_port.Text = "8085";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.bt_cancel_dis);
            this.groupBox1.Controls.Add(this.bt_dis);
            this.groupBox1.Location = new System.Drawing.Point(21, 57);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(196, 100);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Discharge";
            // 
            // bt_cancel_dis
            // 
            this.bt_cancel_dis.Location = new System.Drawing.Point(22, 59);
            this.bt_cancel_dis.Name = "bt_cancel_dis";
            this.bt_cancel_dis.Size = new System.Drawing.Size(152, 23);
            this.bt_cancel_dis.TabIndex = 1;
            this.bt_cancel_dis.Text = "ต้องการ Cancel Discharge";
            this.bt_cancel_dis.UseVisualStyleBackColor = true;
            this.bt_cancel_dis.Click += new System.EventHandler(this.bt_cancel_dis_Click);
            // 
            // bt_dis
            // 
            this.bt_dis.Location = new System.Drawing.Point(22, 24);
            this.bt_dis.Name = "bt_dis";
            this.bt_dis.Size = new System.Drawing.Size(152, 23);
            this.bt_dis.TabIndex = 0;
            this.bt_dis.Text = "ต้องการ Discharge";
            this.bt_dis.UseVisualStyleBackColor = true;
            this.bt_dis.Click += new System.EventHandler(this.bt_dis_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.bt_cancel_trans);
            this.groupBox2.Controls.Add(this.bt_tm_trans);
            this.groupBox2.Controls.Add(this.bt_edit_trans);
            this.groupBox2.Controls.Add(this.bt_trans);
            this.groupBox2.Location = new System.Drawing.Point(21, 163);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(196, 166);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Transition";
            // 
            // bt_cancel_trans
            // 
            this.bt_cancel_trans.Location = new System.Drawing.Point(22, 127);
            this.bt_cancel_trans.Name = "bt_cancel_trans";
            this.bt_cancel_trans.Size = new System.Drawing.Size(152, 23);
            this.bt_cancel_trans.TabIndex = 3;
            this.bt_cancel_trans.Text = "ต้องการยกเลิก Transition";
            this.bt_cancel_trans.UseVisualStyleBackColor = true;
            this.bt_cancel_trans.Click += new System.EventHandler(this.bt_cancel_trans_Click);
            // 
            // bt_tm_trans
            // 
            this.bt_tm_trans.Location = new System.Drawing.Point(22, 93);
            this.bt_tm_trans.Name = "bt_tm_trans";
            this.bt_tm_trans.Size = new System.Drawing.Size(152, 23);
            this.bt_tm_trans.TabIndex = 2;
            this.bt_tm_trans.Text = "ต้องการต่อเวลา Transition";
            this.bt_tm_trans.UseVisualStyleBackColor = true;
            this.bt_tm_trans.Click += new System.EventHandler(this.bt_tm_trans_Click);
            // 
            // bt_edit_trans
            // 
            this.bt_edit_trans.Location = new System.Drawing.Point(22, 58);
            this.bt_edit_trans.Name = "bt_edit_trans";
            this.bt_edit_trans.Size = new System.Drawing.Size(152, 23);
            this.bt_edit_trans.TabIndex = 1;
            this.bt_edit_trans.Text = "ต้องการแก้ไข Transition";
            this.bt_edit_trans.UseVisualStyleBackColor = true;
            this.bt_edit_trans.Click += new System.EventHandler(this.bt_edit_trans_Click);
            // 
            // bt_trans
            // 
            this.bt_trans.Location = new System.Drawing.Point(22, 23);
            this.bt_trans.Name = "bt_trans";
            this.bt_trans.Size = new System.Drawing.Size(152, 23);
            this.bt_trans.TabIndex = 0;
            this.bt_trans.Text = "ต้องการทำ Transition";
            this.bt_trans.UseVisualStyleBackColor = true;
            this.bt_trans.Click += new System.EventHandler(this.bt_trans_Click);
            // 
            // bt_edit_tag_lost
            // 
            this.bt_edit_tag_lost.Location = new System.Drawing.Point(18, 24);
            this.bt_edit_tag_lost.Name = "bt_edit_tag_lost";
            this.bt_edit_tag_lost.Size = new System.Drawing.Size(162, 23);
            this.bt_edit_tag_lost.TabIndex = 8;
            this.bt_edit_tag_lost.Text = "แก้ไขสัญญาณ Tag หาย";
            this.bt_edit_tag_lost.UseVisualStyleBackColor = true;
            this.bt_edit_tag_lost.Click += new System.EventHandler(this.bt_edit_tag_lost_Click);
            // 
            // bt_edit_tag_login
            // 
            this.bt_edit_tag_login.Location = new System.Drawing.Point(18, 59);
            this.bt_edit_tag_login.Name = "bt_edit_tag_login";
            this.bt_edit_tag_login.Size = new System.Drawing.Size(162, 23);
            this.bt_edit_tag_login.TabIndex = 9;
            this.bt_edit_tag_login.Text = "แก้ไขให้ Tag เข้าระบบ";
            this.bt_edit_tag_login.UseVisualStyleBackColor = true;
            this.bt_edit_tag_login.Click += new System.EventHandler(this.bt_edit_tag_login_Click);
            // 
            // bt_edit_room_out
            // 
            this.bt_edit_room_out.Location = new System.Drawing.Point(18, 90);
            this.bt_edit_room_out.Name = "bt_edit_room_out";
            this.bt_edit_room_out.Size = new System.Drawing.Size(162, 43);
            this.bt_edit_room_out.TabIndex = 10;
            this.bt_edit_room_out.Text = "แก้ไขห้องพักมารดา (กรณีแม่กลับบ้าน แต่ทารกยังอยู่)";
            this.bt_edit_room_out.UseVisualStyleBackColor = true;
            this.bt_edit_room_out.Click += new System.EventHandler(this.bt_edit_room_out_Click);
            // 
            // bt_bc_mana
            // 
            this.bt_bc_mana.Location = new System.Drawing.Point(43, 445);
            this.bt_bc_mana.Name = "bt_bc_mana";
            this.bt_bc_mana.Size = new System.Drawing.Size(122, 35);
            this.bt_bc_mana.TabIndex = 11;
            this.bt_bc_mana.Text = "Beacon Management";
            this.bt_bc_mana.UseVisualStyleBackColor = true;
            this.bt_bc_mana.Click += new System.EventHandler(this.bt_bc_mana_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.bt_edit_tag_sn);
            this.groupBox3.Controls.Add(this.bt_edit_tag_lost);
            this.groupBox3.Controls.Add(this.bt_edit_tag_login);
            this.groupBox3.Location = new System.Drawing.Point(237, 160);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 131);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Tag";
            // 
            // bt_edit_tag_sn
            // 
            this.bt_edit_tag_sn.Location = new System.Drawing.Point(18, 93);
            this.bt_edit_tag_sn.Name = "bt_edit_tag_sn";
            this.bt_edit_tag_sn.Size = new System.Drawing.Size(162, 23);
            this.bt_edit_tag_sn.TabIndex = 10;
            this.bt_edit_tag_sn.Text = "แก้ไข Serial Tag";
            this.bt_edit_tag_sn.UseVisualStyleBackColor = true;
            this.bt_edit_tag_sn.Click += new System.EventHandler(this.bt_edit_tag_sn_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.bt_edit_room);
            this.groupBox4.Controls.Add(this.bt_edit_hn_child);
            this.groupBox4.Controls.Add(this.bt_clear_col_db);
            this.groupBox4.Controls.Add(this.bt_edit_room_out);
            this.groupBox4.Location = new System.Drawing.Point(237, 302);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(197, 178);
            this.groupBox4.TabIndex = 13;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "etc";
            // 
            // bt_edit_room
            // 
            this.bt_edit_room.Location = new System.Drawing.Point(18, 57);
            this.bt_edit_room.Name = "bt_edit_room";
            this.bt_edit_room.Size = new System.Drawing.Size(162, 23);
            this.bt_edit_room.TabIndex = 13;
            this.bt_edit_room.Text = "แก้ไขห้องพัก หรือเตียงทารก";
            this.bt_edit_room.UseVisualStyleBackColor = true;
            this.bt_edit_room.Click += new System.EventHandler(this.bt_edit_room_Click);
            // 
            // bt_edit_hn_child
            // 
            this.bt_edit_hn_child.Location = new System.Drawing.Point(18, 24);
            this.bt_edit_hn_child.Name = "bt_edit_hn_child";
            this.bt_edit_hn_child.Size = new System.Drawing.Size(162, 23);
            this.bt_edit_hn_child.TabIndex = 12;
            this.bt_edit_hn_child.Text = "แก้ไข HN ลูก";
            this.bt_edit_hn_child.UseVisualStyleBackColor = true;
            this.bt_edit_hn_child.Click += new System.EventHandler(this.bt_edit_hn_child_Click);
            // 
            // bt_clear_col_db
            // 
            this.bt_clear_col_db.Location = new System.Drawing.Point(18, 143);
            this.bt_clear_col_db.Name = "bt_clear_col_db";
            this.bt_clear_col_db.Size = new System.Drawing.Size(162, 23);
            this.bt_clear_col_db.TabIndex = 11;
            this.bt_clear_col_db.Text = "เคลียร์ข้อมูลที่ไม่จำเป็น";
            this.bt_clear_col_db.UseVisualStyleBackColor = true;
            this.bt_clear_col_db.Click += new System.EventHandler(this.bt_clear_col_db_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(252, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "HN : ";
            // 
            // tb_hn
            // 
            this.tb_hn.Location = new System.Drawing.Point(287, 68);
            this.tb_hn.Name = "tb_hn";
            this.tb_hn.Size = new System.Drawing.Size(100, 20);
            this.tb_hn.TabIndex = 15;
            this.tb_hn.Text = "51450/54";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(252, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Order : ";
            // 
            // tb_ord
            // 
            this.tb_ord.Location = new System.Drawing.Point(300, 98);
            this.tb_ord.Name = "tb_ord";
            this.tb_ord.Size = new System.Drawing.Size(28, 20);
            this.tb_ord.TabIndex = 17;
            this.tb_ord.Text = "-";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(252, 126);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(182, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "*ลูกคนเดียวใช้   -   ลูกแฝดใช้ 1,2,3,4";
            // 
            // bt_add_gate
            // 
            this.bt_add_gate.Enabled = false;
            this.bt_add_gate.Location = new System.Drawing.Point(255, 195);
            this.bt_add_gate.Name = "bt_add_gate";
            this.bt_add_gate.Size = new System.Drawing.Size(150, 23);
            this.bt_add_gate.TabIndex = 8;
            this.bt_add_gate.Text = "ต้องการเพิ่ม Beacon Gate";
            this.bt_add_gate.UseVisualStyleBackColor = true;
            this.bt_add_gate.Click += new System.EventHandler(this.bt_add_gate_Click);
            // 
            // bt_add_bc
            // 
            this.bt_add_bc.Enabled = false;
            this.bt_add_bc.Location = new System.Drawing.Point(255, 124);
            this.bt_add_bc.Name = "bt_add_bc";
            this.bt_add_bc.Size = new System.Drawing.Size(150, 23);
            this.bt_add_bc.TabIndex = 7;
            this.bt_add_bc.Text = "ต้องการเพิ่ม Beacon";
            this.bt_add_bc.UseVisualStyleBackColor = true;
            this.bt_add_bc.Click += new System.EventHandler(this.bt_add_bc_Click);
            // 
            // bt_change_gate
            // 
            this.bt_change_gate.Enabled = false;
            this.bt_change_gate.Location = new System.Drawing.Point(255, 229);
            this.bt_change_gate.Name = "bt_change_gate";
            this.bt_change_gate.Size = new System.Drawing.Size(150, 23);
            this.bt_change_gate.TabIndex = 6;
            this.bt_change_gate.Text = "ต้องการเปลี่ยน Beacon Gate";
            this.bt_change_gate.UseVisualStyleBackColor = true;
            this.bt_change_gate.Click += new System.EventHandler(this.bt_change_gate_Click);
            // 
            // bt_change_bc
            // 
            this.bt_change_bc.Enabled = false;
            this.bt_change_bc.Location = new System.Drawing.Point(255, 158);
            this.bt_change_bc.Name = "bt_change_bc";
            this.bt_change_bc.Size = new System.Drawing.Size(150, 23);
            this.bt_change_bc.TabIndex = 5;
            this.bt_change_bc.Text = "ต้องการเปลี่ยน Beacon";
            this.bt_change_bc.UseVisualStyleBackColor = true;
            this.bt_change_bc.Click += new System.EventHandler(this.bt_change_bc_Click);
            // 
            // bt_check_rssi
            // 
            this.bt_check_rssi.Location = new System.Drawing.Point(68, 182);
            this.bt_check_rssi.Name = "bt_check_rssi";
            this.bt_check_rssi.Size = new System.Drawing.Size(163, 23);
            this.bt_check_rssi.TabIndex = 4;
            this.bt_check_rssi.Text = "เช็คสัญญาณ rssi ของ Tag";
            this.bt_check_rssi.UseVisualStyleBackColor = true;
            this.bt_check_rssi.Click += new System.EventHandler(this.bt_check_rssi_Click);
            // 
            // bt_check_led_and_sound
            // 
            this.bt_check_led_and_sound.Location = new System.Drawing.Point(68, 145);
            this.bt_check_led_and_sound.Name = "bt_check_led_and_sound";
            this.bt_check_led_and_sound.Size = new System.Drawing.Size(163, 23);
            this.bt_check_led_and_sound.TabIndex = 3;
            this.bt_check_led_and_sound.Text = "เช็คไฟ และเสียงของ Beacon";
            this.bt_check_led_and_sound.UseVisualStyleBackColor = true;
            this.bt_check_led_and_sound.Click += new System.EventHandler(this.bt_check_led_and_sound_Click);
            // 
            // bt_back_home
            // 
            this.bt_back_home.Location = new System.Drawing.Point(21, 408);
            this.bt_back_home.Name = "bt_back_home";
            this.bt_back_home.Size = new System.Drawing.Size(75, 23);
            this.bt_back_home.TabIndex = 0;
            this.bt_back_home.Text = "<< Home";
            this.bt_back_home.UseVisualStyleBackColor = true;
            this.bt_back_home.Click += new System.EventHandler(this.bt_back_home_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.bt_resolve_alert);
            this.groupBox5.Controls.Add(this.bt_mute_alert);
            this.groupBox5.Location = new System.Drawing.Point(21, 335);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(196, 96);
            this.groupBox5.TabIndex = 20;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Alert";
            // 
            // bt_resolve_alert
            // 
            this.bt_resolve_alert.Location = new System.Drawing.Point(22, 57);
            this.bt_resolve_alert.Name = "bt_resolve_alert";
            this.bt_resolve_alert.Size = new System.Drawing.Size(152, 23);
            this.bt_resolve_alert.TabIndex = 1;
            this.bt_resolve_alert.Text = "ต้องการ Resolve Alert";
            this.bt_resolve_alert.UseVisualStyleBackColor = true;
            this.bt_resolve_alert.Click += new System.EventHandler(this.bt_resolve_alert_Click);
            // 
            // bt_mute_alert
            // 
            this.bt_mute_alert.Location = new System.Drawing.Point(22, 24);
            this.bt_mute_alert.Name = "bt_mute_alert";
            this.bt_mute_alert.Size = new System.Drawing.Size(152, 23);
            this.bt_mute_alert.TabIndex = 0;
            this.bt_mute_alert.Text = "ต้องการ Mute Alert";
            this.bt_mute_alert.UseVisualStyleBackColor = true;
            this.bt_mute_alert.Click += new System.EventHandler(this.bt_mute_alert_Click);
            // 
            // pn_bc_mana
            // 
            this.pn_bc_mana.Controls.Add(this.pn_trans);
            this.pn_bc_mana.Controls.Add(this.bt_back_home);
            this.pn_bc_mana.Controls.Add(this.bt_add_gate);
            this.pn_bc_mana.Controls.Add(this.bt_add_bc);
            this.pn_bc_mana.Controls.Add(this.bt_change_bc);
            this.pn_bc_mana.Controls.Add(this.bt_check_rssi);
            this.pn_bc_mana.Controls.Add(this.bt_check_led_and_sound);
            this.pn_bc_mana.Controls.Add(this.bt_change_gate);
            this.pn_bc_mana.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_bc_mana.Location = new System.Drawing.Point(0, 0);
            this.pn_bc_mana.Name = "pn_bc_mana";
            this.pn_bc_mana.Size = new System.Drawing.Size(485, 501);
            this.pn_bc_mana.TabIndex = 21;
            this.pn_bc_mana.Visible = false;
            // 
            // pn_trans
            // 
            this.pn_trans.Controls.Add(this.pn_check_R_G);
            this.pn_trans.Controls.Add(this.bt_OK_trans);
            this.pn_trans.Controls.Add(this.cb_trans_to);
            this.pn_trans.Controls.Add(this.cb_trans_from);
            this.pn_trans.Controls.Add(this.label7);
            this.pn_trans.Controls.Add(this.label6);
            this.pn_trans.Controls.Add(this.bt_back_home_trans);
            this.pn_trans.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_trans.Location = new System.Drawing.Point(0, 0);
            this.pn_trans.Name = "pn_trans";
            this.pn_trans.Size = new System.Drawing.Size(485, 501);
            this.pn_trans.TabIndex = 22;
            this.pn_trans.Visible = false;
            // 
            // pn_check_R_G
            // 
            this.pn_check_R_G.Controls.Add(this.pn_check_rssi);
            this.pn_check_R_G.Controls.Add(this.gb_bc_type);
            this.pn_check_R_G.Controls.Add(this.gb_cmd);
            this.pn_check_R_G.Controls.Add(this.lb_show_led);
            this.pn_check_R_G.Controls.Add(this.cb_list_bc);
            this.pn_check_R_G.Controls.Add(this.label9);
            this.pn_check_R_G.Controls.Add(this.bt_back_select_fl);
            this.pn_check_R_G.Controls.Add(this.bt_ok_or_stop);
            this.pn_check_R_G.Controls.Add(this.cb_select_fl);
            this.pn_check_R_G.Controls.Add(this.label8);
            this.pn_check_R_G.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_check_R_G.Location = new System.Drawing.Point(0, 0);
            this.pn_check_R_G.Name = "pn_check_R_G";
            this.pn_check_R_G.Size = new System.Drawing.Size(485, 501);
            this.pn_check_R_G.TabIndex = 7;
            this.pn_check_R_G.Visible = false;
            // 
            // pn_check_rssi
            // 
            this.pn_check_rssi.Controls.Add(this.pn_edit);
            this.pn_check_rssi.Controls.Add(this.lb_timer);
            this.pn_check_rssi.Controls.Add(this.rb_sort_code);
            this.pn_check_rssi.Controls.Add(this.lb_tic_toc);
            this.pn_check_rssi.Controls.Add(this.rb_sort_rssi);
            this.pn_check_rssi.Controls.Add(this.rb_sort_tag);
            this.pn_check_rssi.Controls.Add(this.rb_sort_bc);
            this.pn_check_rssi.Controls.Add(this.label10);
            this.pn_check_rssi.Controls.Add(this.dg_rssi);
            this.pn_check_rssi.Controls.Add(this.gb_rssi_select);
            this.pn_check_rssi.Controls.Add(this.bt_back_rssi);
            this.pn_check_rssi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_check_rssi.Location = new System.Drawing.Point(0, 0);
            this.pn_check_rssi.Name = "pn_check_rssi";
            this.pn_check_rssi.Size = new System.Drawing.Size(485, 501);
            this.pn_check_rssi.TabIndex = 13;
            this.pn_check_rssi.Visible = false;
            // 
            // pn_edit
            // 
            this.pn_edit.Controls.Add(this.pn_edit_room);
            this.pn_edit.Controls.Add(this.label16);
            this.pn_edit.Controls.Add(this.tb_ord_edit_hn_change);
            this.pn_edit.Controls.Add(this.label14);
            this.pn_edit.Controls.Add(this.tb_ord_edit_hn_from);
            this.pn_edit.Controls.Add(this.label15);
            this.pn_edit.Controls.Add(this.rb_edit_hn_child);
            this.pn_edit.Controls.Add(this.rb_edit_sn_tag);
            this.pn_edit.Controls.Add(this.bt_edit_back);
            this.pn_edit.Controls.Add(this.bt_edit_ok);
            this.pn_edit.Controls.Add(this.tb_edit_change);
            this.pn_edit.Controls.Add(this.tb_edit_from);
            this.pn_edit.Controls.Add(this.label13);
            this.pn_edit.Controls.Add(this.label12);
            this.pn_edit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_edit.Location = new System.Drawing.Point(0, 0);
            this.pn_edit.Name = "pn_edit";
            this.pn_edit.Size = new System.Drawing.Size(485, 501);
            this.pn_edit.TabIndex = 8;
            this.pn_edit.Visible = false;
            // 
            // pn_edit_room
            // 
            this.pn_edit_room.Controls.Add(this.tb_edit_room_fl);
            this.pn_edit_room.Controls.Add(this.label18);
            this.pn_edit_room.Controls.Add(this.bt_edit_room_ok);
            this.pn_edit_room.Controls.Add(this.tb_edit_room);
            this.pn_edit_room.Controls.Add(this.label11);
            this.pn_edit_room.Controls.Add(this.bt_edit_room_back);
            this.pn_edit_room.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_edit_room.Location = new System.Drawing.Point(0, 0);
            this.pn_edit_room.Name = "pn_edit_room";
            this.pn_edit_room.Size = new System.Drawing.Size(485, 501);
            this.pn_edit_room.TabIndex = 14;
            this.pn_edit_room.Visible = false;
            // 
            // bt_edit_room_ok
            // 
            this.bt_edit_room_ok.Location = new System.Drawing.Point(293, 266);
            this.bt_edit_room_ok.Name = "bt_edit_room_ok";
            this.bt_edit_room_ok.Size = new System.Drawing.Size(75, 23);
            this.bt_edit_room_ok.TabIndex = 9;
            this.bt_edit_room_ok.Text = "OK";
            this.bt_edit_room_ok.UseVisualStyleBackColor = true;
            this.bt_edit_room_ok.Click += new System.EventHandler(this.bt_edit_room_ok_Click);
            // 
            // tb_edit_room
            // 
            this.tb_edit_room.Location = new System.Drawing.Point(95, 227);
            this.tb_edit_room.Name = "tb_edit_room";
            this.tb_edit_room.Size = new System.Drawing.Size(127, 20);
            this.tb_edit_room.TabIndex = 8;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(91, 186);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(253, 25);
            this.label11.TabIndex = 7;
            this.label11.Text = "เปลี่ยน ห้องพัก/เตียง เป็นหมายเลข";
            // 
            // bt_edit_room_back
            // 
            this.bt_edit_room_back.Location = new System.Drawing.Point(24, 451);
            this.bt_edit_room_back.Name = "bt_edit_room_back";
            this.bt_edit_room_back.Size = new System.Drawing.Size(78, 23);
            this.bt_edit_room_back.TabIndex = 6;
            this.bt_edit_room_back.Text = "<< HOME";
            this.bt_edit_room_back.UseVisualStyleBackColor = true;
            this.bt_edit_room_back.Click += new System.EventHandler(this.bt_edit_room_back_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(201, 258);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(179, 13);
            this.label16.TabIndex = 13;
            this.label16.Text = "* ลูกคนเดียวใช้ -   ลูกแฝดใช้ 1,2,3,4";
            // 
            // tb_ord_edit_hn_change
            // 
            this.tb_ord_edit_hn_change.Location = new System.Drawing.Point(337, 224);
            this.tb_ord_edit_hn_change.Name = "tb_ord_edit_hn_change";
            this.tb_ord_edit_hn_change.Size = new System.Drawing.Size(43, 20);
            this.tb_ord_edit_hn_change.TabIndex = 12;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(291, 227);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(40, 13);
            this.label14.TabIndex = 11;
            this.label14.Text = "order : ";
            // 
            // tb_ord_edit_hn_from
            // 
            this.tb_ord_edit_hn_from.Location = new System.Drawing.Point(337, 179);
            this.tb_ord_edit_hn_from.Name = "tb_ord_edit_hn_from";
            this.tb_ord_edit_hn_from.Size = new System.Drawing.Size(43, 20);
            this.tb_ord_edit_hn_from.TabIndex = 10;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(291, 182);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(40, 13);
            this.label15.TabIndex = 8;
            this.label15.Text = "order : ";
            // 
            // rb_edit_hn_child
            // 
            this.rb_edit_hn_child.AutoSize = true;
            this.rb_edit_hn_child.Location = new System.Drawing.Point(278, 126);
            this.rb_edit_hn_child.Name = "rb_edit_hn_child";
            this.rb_edit_hn_child.Size = new System.Drawing.Size(87, 17);
            this.rb_edit_hn_child.TabIndex = 7;
            this.rb_edit_hn_child.TabStop = true;
            this.rb_edit_hn_child.Text = "แก้ไข HN ลูก";
            this.rb_edit_hn_child.UseVisualStyleBackColor = true;
            this.rb_edit_hn_child.Click += new System.EventHandler(this.rb_edit_hn_child_Click);
            // 
            // rb_edit_sn_tag
            // 
            this.rb_edit_sn_tag.AutoSize = true;
            this.rb_edit_sn_tag.Location = new System.Drawing.Point(143, 125);
            this.rb_edit_sn_tag.Name = "rb_edit_sn_tag";
            this.rb_edit_sn_tag.Size = new System.Drawing.Size(103, 17);
            this.rb_edit_sn_tag.TabIndex = 6;
            this.rb_edit_sn_tag.TabStop = true;
            this.rb_edit_sn_tag.Text = "แก้ไข Serial Tag";
            this.rb_edit_sn_tag.UseVisualStyleBackColor = true;
            this.rb_edit_sn_tag.Click += new System.EventHandler(this.rb_edit_sn_tag_Click);
            // 
            // bt_edit_back
            // 
            this.bt_edit_back.Location = new System.Drawing.Point(24, 457);
            this.bt_edit_back.Name = "bt_edit_back";
            this.bt_edit_back.Size = new System.Drawing.Size(78, 23);
            this.bt_edit_back.TabIndex = 5;
            this.bt_edit_back.Text = "<< HOME";
            this.bt_edit_back.UseVisualStyleBackColor = true;
            this.bt_edit_back.Click += new System.EventHandler(this.bt_edit_back_Click);
            // 
            // bt_edit_ok
            // 
            this.bt_edit_ok.Location = new System.Drawing.Point(302, 302);
            this.bt_edit_ok.Name = "bt_edit_ok";
            this.bt_edit_ok.Size = new System.Drawing.Size(78, 23);
            this.bt_edit_ok.TabIndex = 4;
            this.bt_edit_ok.Text = "OK";
            this.bt_edit_ok.UseVisualStyleBackColor = true;
            this.bt_edit_ok.Click += new System.EventHandler(this.bt_edit_ok_Click);
            // 
            // tb_edit_change
            // 
            this.tb_edit_change.Location = new System.Drawing.Point(171, 223);
            this.tb_edit_change.Name = "tb_edit_change";
            this.tb_edit_change.Size = new System.Drawing.Size(100, 20);
            this.tb_edit_change.TabIndex = 3;
            // 
            // tb_edit_from
            // 
            this.tb_edit_from.Location = new System.Drawing.Point(171, 179);
            this.tb_edit_from.Name = "tb_edit_from";
            this.tb_edit_from.Size = new System.Drawing.Size(100, 20);
            this.tb_edit_from.TabIndex = 2;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(92, 224);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(69, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "เปลี่ยนเป็น : ";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(125, 182);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(34, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "จาก : ";
            // 
            // lb_timer
            // 
            this.lb_timer.AutoSize = true;
            this.lb_timer.Location = new System.Drawing.Point(354, 410);
            this.lb_timer.Name = "lb_timer";
            this.lb_timer.Size = new System.Drawing.Size(26, 13);
            this.lb_timer.TabIndex = 13;
            this.lb_timer.Text = "time";
            // 
            // rb_sort_code
            // 
            this.rb_sort_code.AutoSize = true;
            this.rb_sort_code.Location = new System.Drawing.Point(359, 110);
            this.rb_sort_code.Name = "rb_sort_code";
            this.rb_sort_code.Size = new System.Drawing.Size(49, 17);
            this.rb_sort_code.TabIndex = 12;
            this.rb_sort_code.TabStop = true;
            this.rb_sort_code.Text = "code";
            this.rb_sort_code.UseVisualStyleBackColor = true;
            this.rb_sort_code.CheckedChanged += new System.EventHandler(this.rb_sort_code_CheckedChanged);
            // 
            // lb_tic_toc
            // 
            this.lb_tic_toc.AutoSize = true;
            this.lb_tic_toc.Location = new System.Drawing.Point(356, 392);
            this.lb_tic_toc.Name = "lb_tic_toc";
            this.lb_tic_toc.Size = new System.Drawing.Size(0, 13);
            this.lb_tic_toc.TabIndex = 11;
            // 
            // rb_sort_rssi
            // 
            this.rb_sort_rssi.AutoSize = true;
            this.rb_sort_rssi.Checked = true;
            this.rb_sort_rssi.Location = new System.Drawing.Point(257, 110);
            this.rb_sort_rssi.Name = "rb_sort_rssi";
            this.rb_sort_rssi.Size = new System.Drawing.Size(40, 17);
            this.rb_sort_rssi.TabIndex = 10;
            this.rb_sort_rssi.TabStop = true;
            this.rb_sort_rssi.Text = "rssi";
            this.rb_sort_rssi.UseVisualStyleBackColor = true;
            this.rb_sort_rssi.CheckedChanged += new System.EventHandler(this.rb_sort_rssi_CheckedChanged);
            // 
            // rb_sort_tag
            // 
            this.rb_sort_tag.AutoSize = true;
            this.rb_sort_tag.Location = new System.Drawing.Point(159, 110);
            this.rb_sort_tag.Name = "rb_sort_tag";
            this.rb_sort_tag.Size = new System.Drawing.Size(40, 17);
            this.rb_sort_tag.TabIndex = 9;
            this.rb_sort_tag.Text = "tag";
            this.rb_sort_tag.UseVisualStyleBackColor = true;
            this.rb_sort_tag.CheckedChanged += new System.EventHandler(this.rb_sort_tag_CheckedChanged);
            // 
            // rb_sort_bc
            // 
            this.rb_sort_bc.AutoSize = true;
            this.rb_sort_bc.Location = new System.Drawing.Point(59, 110);
            this.rb_sort_bc.Name = "rb_sort_bc";
            this.rb_sort_bc.Size = new System.Drawing.Size(61, 17);
            this.rb_sort_bc.TabIndex = 8;
            this.rb_sort_bc.Text = "beacon";
            this.rb_sort_bc.UseVisualStyleBackColor = true;
            this.rb_sort_bc.CheckedChanged += new System.EventHandler(this.rb_sort_bc_CheckedChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(5, 112);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(46, 13);
            this.label10.TabIndex = 7;
            this.label10.Text = "Sort by :";
            // 
            // dg_rssi
            // 
            this.dg_rssi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_rssi.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_bc,
            this.col_tag,
            this.col_rssi,
            this.Event,
            this.col_code});
            this.dg_rssi.Location = new System.Drawing.Point(8, 141);
            this.dg_rssi.Name = "dg_rssi";
            this.dg_rssi.Size = new System.Drawing.Size(445, 244);
            this.dg_rssi.TabIndex = 6;
            // 
            // col_bc
            // 
            this.col_bc.HeaderText = "Beacon";
            this.col_bc.Name = "col_bc";
            // 
            // col_tag
            // 
            this.col_tag.HeaderText = "Tag";
            this.col_tag.Name = "col_tag";
            // 
            // col_rssi
            // 
            this.col_rssi.HeaderText = "RSSI";
            this.col_rssi.Name = "col_rssi";
            // 
            // Event
            // 
            this.Event.HeaderText = "Event";
            this.Event.Name = "Event";
            // 
            // col_code
            // 
            this.col_code.HeaderText = "Code";
            this.col_code.Name = "col_code";
            // 
            // gb_rssi_select
            // 
            this.gb_rssi_select.Controls.Add(this.cb_rssi_select_tag);
            this.gb_rssi_select.Controls.Add(this.cb_rssi_select_bc);
            this.gb_rssi_select.Controls.Add(this.rb_rssi_select_tag);
            this.gb_rssi_select.Controls.Add(this.rb_rssi_select_bc);
            this.gb_rssi_select.Location = new System.Drawing.Point(55, 8);
            this.gb_rssi_select.Name = "gb_rssi_select";
            this.gb_rssi_select.Size = new System.Drawing.Size(342, 89);
            this.gb_rssi_select.TabIndex = 5;
            this.gb_rssi_select.TabStop = false;
            this.gb_rssi_select.Text = "Select";
            // 
            // cb_rssi_select_tag
            // 
            this.cb_rssi_select_tag.FormattingEnabled = true;
            this.cb_rssi_select_tag.Location = new System.Drawing.Point(188, 50);
            this.cb_rssi_select_tag.Name = "cb_rssi_select_tag";
            this.cb_rssi_select_tag.Size = new System.Drawing.Size(121, 21);
            this.cb_rssi_select_tag.TabIndex = 3;
            this.cb_rssi_select_tag.Text = "-- Selected --";
            // 
            // cb_rssi_select_bc
            // 
            this.cb_rssi_select_bc.Enabled = false;
            this.cb_rssi_select_bc.FormattingEnabled = true;
            this.cb_rssi_select_bc.Location = new System.Drawing.Point(29, 50);
            this.cb_rssi_select_bc.Name = "cb_rssi_select_bc";
            this.cb_rssi_select_bc.Size = new System.Drawing.Size(121, 21);
            this.cb_rssi_select_bc.TabIndex = 2;
            this.cb_rssi_select_bc.Text = "-- Selected --";
            // 
            // rb_rssi_select_tag
            // 
            this.rb_rssi_select_tag.AutoSize = true;
            this.rb_rssi_select_tag.Checked = true;
            this.rb_rssi_select_tag.Location = new System.Drawing.Point(187, 21);
            this.rb_rssi_select_tag.Name = "rb_rssi_select_tag";
            this.rb_rssi_select_tag.Size = new System.Drawing.Size(44, 17);
            this.rb_rssi_select_tag.TabIndex = 1;
            this.rb_rssi_select_tag.TabStop = true;
            this.rb_rssi_select_tag.Text = "Tag";
            this.rb_rssi_select_tag.UseVisualStyleBackColor = true;
            this.rb_rssi_select_tag.CheckedChanged += new System.EventHandler(this.rb_rssi_select_tag_CheckedChanged);
            // 
            // rb_rssi_select_bc
            // 
            this.rb_rssi_select_bc.AutoSize = true;
            this.rb_rssi_select_bc.Location = new System.Drawing.Point(28, 23);
            this.rb_rssi_select_bc.Name = "rb_rssi_select_bc";
            this.rb_rssi_select_bc.Size = new System.Drawing.Size(62, 17);
            this.rb_rssi_select_bc.TabIndex = 0;
            this.rb_rssi_select_bc.Text = "Beacon";
            this.rb_rssi_select_bc.UseVisualStyleBackColor = true;
            this.rb_rssi_select_bc.CheckedChanged += new System.EventHandler(this.rb_rssi_select_bc_CheckedChanged);
            // 
            // bt_back_rssi
            // 
            this.bt_back_rssi.Location = new System.Drawing.Point(21, 407);
            this.bt_back_rssi.Name = "bt_back_rssi";
            this.bt_back_rssi.Size = new System.Drawing.Size(75, 23);
            this.bt_back_rssi.TabIndex = 4;
            this.bt_back_rssi.Text = "<< Home";
            this.bt_back_rssi.UseVisualStyleBackColor = true;
            this.bt_back_rssi.Click += new System.EventHandler(this.bt_back_rssi_Click);
            // 
            // gb_bc_type
            // 
            this.gb_bc_type.Controls.Add(this.rb_t);
            this.gb_bc_type.Controls.Add(this.rb_l);
            this.gb_bc_type.Controls.Add(this.rb_b);
            this.gb_bc_type.Controls.Add(this.rb_h);
            this.gb_bc_type.Controls.Add(this.rb_p);
            this.gb_bc_type.Controls.Add(this.rb_n);
            this.gb_bc_type.Controls.Add(this.rb_x);
            this.gb_bc_type.Controls.Add(this.rb_g);
            this.gb_bc_type.Controls.Add(this.rb_r);
            this.gb_bc_type.Location = new System.Drawing.Point(95, 43);
            this.gb_bc_type.Name = "gb_bc_type";
            this.gb_bc_type.Size = new System.Drawing.Size(266, 97);
            this.gb_bc_type.TabIndex = 12;
            this.gb_bc_type.TabStop = false;
            this.gb_bc_type.Text = "Select Beacon Type";
            // 
            // rb_t
            // 
            this.rb_t.AutoSize = true;
            this.rb_t.Location = new System.Drawing.Point(145, 65);
            this.rb_t.Name = "rb_t";
            this.rb_t.Size = new System.Drawing.Size(32, 17);
            this.rb_t.TabIndex = 8;
            this.rb_t.Text = "T";
            this.rb_t.UseVisualStyleBackColor = true;
            this.rb_t.CheckedChanged += new System.EventHandler(this.rb_t_CheckedChanged);
            // 
            // rb_l
            // 
            this.rb_l.AutoSize = true;
            this.rb_l.Location = new System.Drawing.Point(175, 30);
            this.rb_l.Name = "rb_l";
            this.rb_l.Size = new System.Drawing.Size(31, 17);
            this.rb_l.TabIndex = 7;
            this.rb_l.Text = "L";
            this.rb_l.UseVisualStyleBackColor = true;
            this.rb_l.CheckedChanged += new System.EventHandler(this.rb_l_CheckedChanged);
            // 
            // rb_b
            // 
            this.rb_b.AutoSize = true;
            this.rb_b.Location = new System.Drawing.Point(16, 30);
            this.rb_b.Name = "rb_b";
            this.rb_b.Size = new System.Drawing.Size(32, 17);
            this.rb_b.TabIndex = 6;
            this.rb_b.Text = "B";
            this.rb_b.UseVisualStyleBackColor = true;
            this.rb_b.CheckedChanged += new System.EventHandler(this.rb_b_CheckedChanged);
            // 
            // rb_h
            // 
            this.rb_h.AutoSize = true;
            this.rb_h.Location = new System.Drawing.Point(122, 30);
            this.rb_h.Name = "rb_h";
            this.rb_h.Size = new System.Drawing.Size(33, 17);
            this.rb_h.TabIndex = 5;
            this.rb_h.Text = "H";
            this.rb_h.UseVisualStyleBackColor = true;
            this.rb_h.CheckedChanged += new System.EventHandler(this.rb_h_CheckedChanged);
            // 
            // rb_p
            // 
            this.rb_p.AutoSize = true;
            this.rb_p.Location = new System.Drawing.Point(38, 65);
            this.rb_p.Name = "rb_p";
            this.rb_p.Size = new System.Drawing.Size(32, 17);
            this.rb_p.TabIndex = 4;
            this.rb_p.Text = "P";
            this.rb_p.UseVisualStyleBackColor = true;
            this.rb_p.CheckedChanged += new System.EventHandler(this.rb_p_CheckedChanged);
            // 
            // rb_n
            // 
            this.rb_n.AutoSize = true;
            this.rb_n.Location = new System.Drawing.Point(224, 30);
            this.rb_n.Name = "rb_n";
            this.rb_n.Size = new System.Drawing.Size(33, 17);
            this.rb_n.TabIndex = 3;
            this.rb_n.Text = "N";
            this.rb_n.UseVisualStyleBackColor = true;
            this.rb_n.CheckedChanged += new System.EventHandler(this.rb_n_CheckedChanged);
            // 
            // rb_x
            // 
            this.rb_x.AutoSize = true;
            this.rb_x.Location = new System.Drawing.Point(194, 65);
            this.rb_x.Name = "rb_x";
            this.rb_x.Size = new System.Drawing.Size(32, 17);
            this.rb_x.TabIndex = 2;
            this.rb_x.Text = "X";
            this.rb_x.UseVisualStyleBackColor = true;
            this.rb_x.CheckedChanged += new System.EventHandler(this.rb_x_CheckedChanged);
            // 
            // rb_g
            // 
            this.rb_g.AutoSize = true;
            this.rb_g.Location = new System.Drawing.Point(67, 30);
            this.rb_g.Name = "rb_g";
            this.rb_g.Size = new System.Drawing.Size(33, 17);
            this.rb_g.TabIndex = 1;
            this.rb_g.Text = "G";
            this.rb_g.UseVisualStyleBackColor = true;
            this.rb_g.CheckedChanged += new System.EventHandler(this.rb_g_CheckedChanged);
            // 
            // rb_r
            // 
            this.rb_r.AutoSize = true;
            this.rb_r.Checked = true;
            this.rb_r.Location = new System.Drawing.Point(92, 65);
            this.rb_r.Name = "rb_r";
            this.rb_r.Size = new System.Drawing.Size(33, 17);
            this.rb_r.TabIndex = 0;
            this.rb_r.TabStop = true;
            this.rb_r.Text = "R";
            this.rb_r.UseVisualStyleBackColor = true;
            this.rb_r.CheckedChanged += new System.EventHandler(this.rb_r_CheckedChanged);
            // 
            // gb_cmd
            // 
            this.gb_cmd.Controls.Add(this.rb_reset);
            this.gb_cmd.Controls.Add(this.rb_on_work);
            this.gb_cmd.Controls.Add(this.rb_on_led);
            this.gb_cmd.Controls.Add(this.rb_off_led);
            this.gb_cmd.Controls.Add(this.rb_off_sound);
            this.gb_cmd.Controls.Add(this.rb_on_sound);
            this.gb_cmd.Location = new System.Drawing.Point(81, 155);
            this.gb_cmd.Name = "gb_cmd";
            this.gb_cmd.Size = new System.Drawing.Size(292, 87);
            this.gb_cmd.TabIndex = 11;
            this.gb_cmd.TabStop = false;
            this.gb_cmd.Text = "Select Command";
            // 
            // rb_reset
            // 
            this.rb_reset.AutoSize = true;
            this.rb_reset.Location = new System.Drawing.Point(171, 58);
            this.rb_reset.Name = "rb_reset";
            this.rb_reset.Size = new System.Drawing.Size(71, 17);
            this.rb_reset.TabIndex = 11;
            this.rb_reset.TabStop = true;
            this.rb_reset.Text = "คืนค่าเดิม";
            this.rb_reset.UseVisualStyleBackColor = true;
            // 
            // rb_on_work
            // 
            this.rb_on_work.AutoSize = true;
            this.rb_on_work.Location = new System.Drawing.Point(171, 30);
            this.rb_on_work.Name = "rb_on_work";
            this.rb_on_work.Size = new System.Drawing.Size(110, 17);
            this.rb_on_work.TabIndex = 10;
            this.rb_on_work.TabStop = true;
            this.rb_on_work.Text = "สั่งงานตามประเภท";
            this.rb_on_work.UseVisualStyleBackColor = true;
            // 
            // rb_on_led
            // 
            this.rb_on_led.AutoSize = true;
            this.rb_on_led.Checked = true;
            this.rb_on_led.Location = new System.Drawing.Point(13, 29);
            this.rb_on_led.Name = "rb_on_led";
            this.rb_on_led.Size = new System.Drawing.Size(58, 17);
            this.rb_on_led.TabIndex = 6;
            this.rb_on_led.TabStop = true;
            this.rb_on_led.Text = "เปิดไฟ";
            this.rb_on_led.UseVisualStyleBackColor = true;
            // 
            // rb_off_led
            // 
            this.rb_off_led.AutoSize = true;
            this.rb_off_led.Location = new System.Drawing.Point(13, 58);
            this.rb_off_led.Name = "rb_off_led";
            this.rb_off_led.Size = new System.Drawing.Size(53, 17);
            this.rb_off_led.TabIndex = 7;
            this.rb_off_led.Text = "ปิดไฟ";
            this.rb_off_led.UseVisualStyleBackColor = true;
            // 
            // rb_off_sound
            // 
            this.rb_off_sound.AutoSize = true;
            this.rb_off_sound.Location = new System.Drawing.Point(90, 58);
            this.rb_off_sound.Name = "rb_off_sound";
            this.rb_off_sound.Size = new System.Drawing.Size(62, 17);
            this.rb_off_sound.TabIndex = 9;
            this.rb_off_sound.Text = "ปิดเสียง";
            this.rb_off_sound.UseVisualStyleBackColor = true;
            // 
            // rb_on_sound
            // 
            this.rb_on_sound.AutoSize = true;
            this.rb_on_sound.Location = new System.Drawing.Point(90, 29);
            this.rb_on_sound.Name = "rb_on_sound";
            this.rb_on_sound.Size = new System.Drawing.Size(67, 17);
            this.rb_on_sound.TabIndex = 8;
            this.rb_on_sound.Text = "เปิดเสียง";
            this.rb_on_sound.UseVisualStyleBackColor = true;
            // 
            // lb_show_led
            // 
            this.lb_show_led.AutoSize = true;
            this.lb_show_led.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_show_led.ForeColor = System.Drawing.Color.Black;
            this.lb_show_led.Location = new System.Drawing.Point(179, 359);
            this.lb_show_led.Name = "lb_show_led";
            this.lb_show_led.Size = new System.Drawing.Size(102, 20);
            this.lb_show_led.TabIndex = 10;
            this.lb_show_led.Text = "ON : GREEN";
            this.lb_show_led.Visible = false;
            // 
            // cb_list_bc
            // 
            this.cb_list_bc.FormattingEnabled = true;
            this.cb_list_bc.Location = new System.Drawing.Point(284, 266);
            this.cb_list_bc.Name = "cb_list_bc";
            this.cb_list_bc.Size = new System.Drawing.Size(121, 21);
            this.cb_list_bc.TabIndex = 5;
            this.cb_list_bc.Text = "-- Selected --";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(228, 269);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "Beacon :";
            // 
            // bt_back_select_fl
            // 
            this.bt_back_select_fl.Location = new System.Drawing.Point(21, 407);
            this.bt_back_select_fl.Name = "bt_back_select_fl";
            this.bt_back_select_fl.Size = new System.Drawing.Size(75, 23);
            this.bt_back_select_fl.TabIndex = 3;
            this.bt_back_select_fl.Text = "<< Home";
            this.bt_back_select_fl.UseVisualStyleBackColor = true;
            this.bt_back_select_fl.Click += new System.EventHandler(this.bt_back_select_fl_Click);
            // 
            // bt_ok_or_stop
            // 
            this.bt_ok_or_stop.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bt_ok_or_stop.Location = new System.Drawing.Point(191, 318);
            this.bt_ok_or_stop.Name = "bt_ok_or_stop";
            this.bt_ok_or_stop.Size = new System.Drawing.Size(75, 23);
            this.bt_ok_or_stop.TabIndex = 2;
            this.bt_ok_or_stop.Text = "OK";
            this.bt_ok_or_stop.UseVisualStyleBackColor = true;
            this.bt_ok_or_stop.Click += new System.EventHandler(this.bt_ok_or_stop_Click);
            // 
            // cb_select_fl
            // 
            this.cb_select_fl.FormattingEnabled = true;
            this.cb_select_fl.Location = new System.Drawing.Point(90, 266);
            this.cb_select_fl.Name = "cb_select_fl";
            this.cb_select_fl.Size = new System.Drawing.Size(121, 21);
            this.cb_select_fl.TabIndex = 1;
            this.cb_select_fl.Text = "-- Selected --";
            this.cb_select_fl.SelectedIndexChanged += new System.EventHandler(this.cb_select_fl_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(56, 269);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(28, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "ชั้น :";
            // 
            // bt_OK_trans
            // 
            this.bt_OK_trans.Location = new System.Drawing.Point(234, 244);
            this.bt_OK_trans.Name = "bt_OK_trans";
            this.bt_OK_trans.Size = new System.Drawing.Size(75, 23);
            this.bt_OK_trans.TabIndex = 6;
            this.bt_OK_trans.Text = "OK";
            this.bt_OK_trans.UseVisualStyleBackColor = true;
            this.bt_OK_trans.Click += new System.EventHandler(this.bt_OK_trans_Click);
            // 
            // cb_trans_to
            // 
            this.cb_trans_to.FormattingEnabled = true;
            this.cb_trans_to.Location = new System.Drawing.Point(188, 190);
            this.cb_trans_to.Name = "cb_trans_to";
            this.cb_trans_to.Size = new System.Drawing.Size(121, 21);
            this.cb_trans_to.TabIndex = 5;
            this.cb_trans_to.Text = "-- Selected --";
            // 
            // cb_trans_from
            // 
            this.cb_trans_from.FormattingEnabled = true;
            this.cb_trans_from.Location = new System.Drawing.Point(188, 131);
            this.cb_trans_from.Name = "cb_trans_from";
            this.cb_trans_from.Size = new System.Drawing.Size(121, 21);
            this.cb_trans_from.TabIndex = 4;
            this.cb_trans_from.Text = "-- Selected --";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(137, 193);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "ไปยัง :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(137, 134);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "จาก :";
            // 
            // bt_back_home_trans
            // 
            this.bt_back_home_trans.Location = new System.Drawing.Point(27, 407);
            this.bt_back_home_trans.Name = "bt_back_home_trans";
            this.bt_back_home_trans.Size = new System.Drawing.Size(75, 23);
            this.bt_back_home_trans.TabIndex = 1;
            this.bt_back_home_trans.Text = "<< Home";
            this.bt_back_home_trans.UseVisualStyleBackColor = true;
            this.bt_back_home_trans.Click += new System.EventHandler(this.bt_back_home_trans_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(257, 144);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(59, 13);
            this.label17.TabIndex = 21;
            this.label17.Text = "มารดาใช้ 0";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(238, 225);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(35, 20);
            this.label18.TabIndex = 10;
            this.label18.Text = "ชั้น :";
            // 
            // tb_edit_room_fl
            // 
            this.tb_edit_room_fl.Location = new System.Drawing.Point(284, 224);
            this.tb_edit_room_fl.Name = "tb_edit_room_fl";
            this.tb_edit_room_fl.Size = new System.Drawing.Size(84, 20);
            this.tb_edit_room_fl.TabIndex = 11;
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(485, 501);
            this.Controls.Add(this.pn_bc_mana);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tb_ord);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tb_hn);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.bt_bc_mana);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tb_port);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cb_hos);
            this.Name = "main";
            this.Text = "Baby Tag Management";
            this.Load += new System.EventHandler(this.main_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.pn_bc_mana.ResumeLayout(false);
            this.pn_trans.ResumeLayout(false);
            this.pn_trans.PerformLayout();
            this.pn_check_R_G.ResumeLayout(false);
            this.pn_check_R_G.PerformLayout();
            this.pn_check_rssi.ResumeLayout(false);
            this.pn_check_rssi.PerformLayout();
            this.pn_edit.ResumeLayout(false);
            this.pn_edit.PerformLayout();
            this.pn_edit_room.ResumeLayout(false);
            this.pn_edit_room.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_rssi)).EndInit();
            this.gb_rssi_select.ResumeLayout(false);
            this.gb_rssi_select.PerformLayout();
            this.gb_bc_type.ResumeLayout(false);
            this.gb_bc_type.PerformLayout();
            this.gb_cmd.ResumeLayout(false);
            this.gb_cmd.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox cb_hos;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_port;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button bt_cancel_dis;
        private System.Windows.Forms.Button bt_dis;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button bt_cancel_trans;
        private System.Windows.Forms.Button bt_tm_trans;
        private System.Windows.Forms.Button bt_edit_trans;
        private System.Windows.Forms.Button bt_trans;
        private System.Windows.Forms.Button bt_edit_tag_lost;
        private System.Windows.Forms.Button bt_edit_tag_login;
        private System.Windows.Forms.Button bt_edit_room_out;
        private System.Windows.Forms.Button bt_bc_mana;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_hn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tb_ord;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button bt_add_gate;
        private System.Windows.Forms.Button bt_add_bc;
        private System.Windows.Forms.Button bt_change_gate;
        private System.Windows.Forms.Button bt_change_bc;
        private System.Windows.Forms.Button bt_check_rssi;
        private System.Windows.Forms.Button bt_check_led_and_sound;
        private System.Windows.Forms.Button bt_back_home;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button bt_resolve_alert;
        private System.Windows.Forms.Button bt_mute_alert;
        private System.Windows.Forms.Panel pn_bc_mana;
        private System.Windows.Forms.Panel pn_trans;
        private System.Windows.Forms.Button bt_OK_trans;
        private System.Windows.Forms.ComboBox cb_trans_to;
        private System.Windows.Forms.ComboBox cb_trans_from;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button bt_back_home_trans;
        private System.Windows.Forms.Button bt_clear_col_db;
        private System.Windows.Forms.Panel pn_check_R_G;
        private System.Windows.Forms.Button bt_ok_or_stop;
        private System.Windows.Forms.ComboBox cb_select_fl;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button bt_back_select_fl;
        private System.Windows.Forms.RadioButton rb_off_led;
        private System.Windows.Forms.RadioButton rb_on_led;
        private System.Windows.Forms.ComboBox cb_list_bc;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RadioButton rb_off_sound;
        private System.Windows.Forms.RadioButton rb_on_sound;
        private System.Windows.Forms.Label lb_show_led;
        private System.Windows.Forms.GroupBox gb_bc_type;
        private System.Windows.Forms.RadioButton rb_t;
        private System.Windows.Forms.RadioButton rb_l;
        private System.Windows.Forms.RadioButton rb_b;
        private System.Windows.Forms.RadioButton rb_h;
        private System.Windows.Forms.RadioButton rb_p;
        private System.Windows.Forms.RadioButton rb_n;
        private System.Windows.Forms.RadioButton rb_x;
        private System.Windows.Forms.RadioButton rb_g;
        private System.Windows.Forms.RadioButton rb_r;
        private System.Windows.Forms.GroupBox gb_cmd;
        private System.Windows.Forms.RadioButton rb_reset;
        private System.Windows.Forms.RadioButton rb_on_work;
        private System.Windows.Forms.Panel pn_check_rssi;
        private System.Windows.Forms.Button bt_back_rssi;
        private System.Windows.Forms.GroupBox gb_rssi_select;
        private System.Windows.Forms.RadioButton rb_rssi_select_tag;
        private System.Windows.Forms.RadioButton rb_rssi_select_bc;
        private System.Windows.Forms.ComboBox cb_rssi_select_tag;
        private System.Windows.Forms.ComboBox cb_rssi_select_bc;
        private System.Windows.Forms.RadioButton rb_sort_rssi;
        private System.Windows.Forms.RadioButton rb_sort_tag;
        private System.Windows.Forms.RadioButton rb_sort_bc;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView dg_rssi;
        private System.Windows.Forms.Label lb_tic_toc;
        private System.Windows.Forms.RadioButton rb_sort_code;
        private System.Windows.Forms.Label lb_timer;
        private System.Windows.Forms.Button bt_edit_tag_sn;
        private System.Windows.Forms.Button bt_edit_hn_child;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_bc;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_tag;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_rssi;
        private System.Windows.Forms.DataGridViewTextBoxColumn Event;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_code;
        private System.Windows.Forms.Panel pn_edit;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tb_ord_edit_hn_change;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tb_ord_edit_hn_from;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.RadioButton rb_edit_hn_child;
        private System.Windows.Forms.RadioButton rb_edit_sn_tag;
        private System.Windows.Forms.Button bt_edit_back;
        private System.Windows.Forms.Button bt_edit_ok;
        private System.Windows.Forms.TextBox tb_edit_change;
        private System.Windows.Forms.TextBox tb_edit_from;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button bt_edit_room;
        private System.Windows.Forms.Panel pn_edit_room;
        private System.Windows.Forms.Button bt_edit_room_back;
        private System.Windows.Forms.Button bt_edit_room_ok;
        private System.Windows.Forms.TextBox tb_edit_room;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tb_edit_room_fl;
        private System.Windows.Forms.Label label18;
    }
}

